<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package hyper-commerce
 */

?><!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-120986095-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-120986095-1');
</script>

  <meta charset="<?php bloginfo( 'charset' ); ?>">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="profile" href="http://gmpg.org/xfn/11">    
  <?php wp_head(); ?>  
  
</head>
<body <?php body_class(); ?>>
  <div id="page" class="hfeed site">
    <header id="masthead" class="site-header" role="banner">
      <div class="header-top">
        <div class="container">
          <div class="row">
            <div class="col-lg-3 col-md-2 col-sm-2">
			<div class="site-branding hidden-xs">
            <?php
              the_custom_logo(); ?>
                <h1 class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></h1>
                                    <?php
                                        $description = get_bloginfo( 'description', 'display' );
                                        if ( $description || is_customize_preview() ) : ?>
                                            <p class="site-description"><?php echo esc_html( $description ); /* WPCS: xss ok. */ ?></p>
                                    <?php
                                        endif;
                                    ?>                
          	</div>              
            </div>
            <div class="col-lg-5 col-md-6 col-sm-5">
              <?php  
                if( get_theme_mod( 'hypercommerce_searchbar_setting', 0 ) == 1 ){
              ?>
              <div class="search-for">
                <?php /*get_search_form();*/ echo do_shortcode("[wcas-search-form]"); ?>
              </div>
              <?php } ?>
            </div>

          <div class="col-lg-3 col-lg-offset-1 col-sm-5 col-md-4">
            <?php  
              if( get_theme_mod( 'hypercommerce_woo_mycart_dropdown_setting', 1 ) == 1 ){
                if( hypercommerce_woocommerce_activated() && (is_woocommerce() || is_cart() || is_checkout() || is_front_page() ) ){
            ?>
            <div class="cart-account">
              <!--Custom cart start-->
              <div class="header-cart">
                
                </div>
                <!--Custom cart end-->
                </div>
                <?php } } ?>
              </div>
            </div>
          </div>
        </div>

        <div class="header-bottom">
          <div class="container">
            <div class="row">
              <div class="site-branding hidden-lg hidden-md hidden-sm">
                    <?php
                     the_custom_logo(); ?>
                     <h1 class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></h1>
					<?php
                        $description = get_bloginfo( 'description', 'display' );
                        if ( $description || is_customize_preview() ) : ?>
                            <p class="site-description"><?php echo esc_html( $description ); /* WPCS: xss ok. */ ?></p>
                    <?php endif; ?>                
               </div>              
              <nav id="site-navigation" class="main-navigation" role="navigation">
                <div class="navbar navbar-default">
                  <div class="navbar-header">
                    <button type="button" data-toggle="collapse" data-target="#navbar-collapse-grid" class="navbar-toggle">
                      <span class="icon-bar"></span>
                      <span class="icon-bar"></span>
                      <span class="icon-bar"></span>
                    </button>
                  </div>                  
                  <div id="navbar-collapse-grid" class="navbar-collapse collapse">                    
					  <?php
                        wp_nav_menu( array(
                          'theme_location' => 'menu-1',
                          'menu_id'        => 'primary-menu',
                          'container'      => 'ul',
                          'depth'           => '3',
						  'fallback_cb'       => 'WP_Bootstrap_Navwalker::fallback',
                          'menu_class'     => 'nav navbar-nav',
                          'item_wrap'      => '<ul class="%2$s">%3s</ul>',
                          'walker'         => new wp_bootstrap_navwalker()
                        ) );
                      ?>                        
          </div>
        </div>
      </nav>



    </div>
  </div>
</div>
</header>