<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package hyper-commerce
 */

get_header(); ?>

<div id="content" class="site-content">
  <div class="container">
    <div class="row">
      <div id="content" class="site-content">
        <div class="container">
          <div class="row">
            <div class="col-lg-8 col-md-8 col-lg-offset-2 col-md-offset-2 col-sm-12 col-xs-12">
              <div id="primary" class="content-area">
                <main id="main" class="site-main">
                  
					<section class="error-404 not-found">
							<h1 class="page-title"><?php esc_html_e( '404', 'hyper-commerce' ); ?></h1>
							<h2 class="page-title"><?php esc_html_e( 'Oops! That page can&rsquo;t be found.', 'hyper-commerce' ); ?></h2>
							<p><?php esc_html_e( 'Go back to the homepage.', 'hyper-commerce' ); ?></p>
                                    <a class="home" href="<?php echo esc_url( home_url( '/' ) ); ?>"><?php esc_html_e( 'Homepage', 'hyper-commerce' ); ?></a>  
					</section><!-- .error-404 -->					       
                  
                </main>
              </div>
            </div>
      
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</div>

<?php get_footer(); ?>