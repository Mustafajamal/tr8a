<?php
/**
 * Template part for displaying featured products on custom homepage.
 *
 * @package hyper-commerce
 */
?>

			<section class="featured_product" id="featureproducts">
                <?php  
                    $section_title = get_theme_mod( 'hypercommerce_woo_featured_p_title_setting', __('Featured Products', 'hyper-commerce' ) );
                ?>
              <h2 class="section-title"><?php echo esc_html( $section_title ); ?></h2>          
              <div class="woocommerce">
                <ul class="products">
                
					<?php
                    $products_listing = get_theme_mod( 'hypercommerce_woo_featured_p_list_setting', 4 );
					$count = 1;
                    $args = array(
                        'post_type' => 'product',
                        'posts_per_page' => $products_listing,
                        'tax_query' => array(
                                array(
                                    'taxonomy' => 'product_visibility',
                                    'field'    => 'name',
                                    'terms'    => 'featured',
                                ),
                            ),
                        );
                    $loop = new WP_Query( $args );
                    if ( $loop->have_posts() ) {
                    while ( $loop->have_posts() ) : $loop->the_post();
					
						wc_get_template_part( 'content', 'product' );
					
					if ($count % 4 == 0) {
						echo "<div class='clearfix hidden-xs'></div>";
					}
					$count++;
					endwhile;
					} else {
						echo esc_html__( 'No products found', 'hyper-commerce' );
					}
					wp_reset_postdata();
            ?>                        
                        
                </ul>
               </div>
              </section>