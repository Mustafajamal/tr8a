<?php
/**
 * Template part for displaying slider on custom homepage.
 *
 * @package hyper-commerce
 */
?>

<div class="top-section">
    <div class="container">
      <div class="row">
        <div class="col-lg-3 col-md-3 hidden-xs hidden-xm">
	        <div class="widget-area equalheight">
				<?php dynamic_sidebar( 'homepage-aside' ); ?>          
            </div>
        </div>

        <div class="col-lg-9 col-md-9">
          <div class="flexslider">
            <ul class="slides">
              
			  <?php
              $theme_tag = get_theme_mod( 'hypercommerce_woo_slider_tag_setting', 'featured' );
              $button_label = get_theme_mod( 'hypercommerce_woo_slider_button_label', 'See Details' );
                $args = array(
                  'posts_per_page' => 6,
				  'post_type' => 'product',
                  'tax_query' =>  array(
                    array(
                    'taxonomy' => 'product_tag',
                    'terms' => $theme_tag,
                    'field' =>  'slug',
                  )
                  )
        
                    );
                $loop = new WP_Query( $args );
                if( $loop -> have_posts() ) :
                    while( $loop -> have_posts() ) : $loop -> the_post();
              ?>              
              
                  <li>
                    <div class="overlay hidden-xs"></div>
   	                <a href="<?php the_permalink(); ?>"><?php the_post_thumbnail('hypercommerce-featured-single', array('class' => 'img-responsive')); ?></a>                    
                    <div class="caption">
                      <h2><?php the_title(); ?></h2>
                      <a href="<?php the_permalink(); ?>"  class="btn btn-shop">
                        <?php echo esc_html($button_label); ?>
                      </a>
                    </div>
                  </li>
              
				<?php
                endwhile;
                endif;
                wp_reset_postdata();
                ?>              
                                      
            </ul>
          </div>
        </div>
      </div>
    </div>
</div>	