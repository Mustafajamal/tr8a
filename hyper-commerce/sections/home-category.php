<?php
/**
 * Template part for displaying product categories on custom homepage.
 *
 * @package hyper-commerce
 */
?>

<section class="shop_category">
              <h2 class="section-title"><?php echo esc_html(get_theme_mod( 'hypercommerce_woo_featured_title_setting', __('Shop By Categories', 'hyper-commerce') ) ); ?></h2>
				<?php
                $cat1 = get_theme_mod( 'hypercommerce_woo_featured_first_list_setting' );
                $cat2 = get_theme_mod( 'hypercommerce_woo_featured_second_list_setting' );
                $cat3 = get_theme_mod( 'hypercommerce_woo_featured_third_list_setting' );
                $cat_array = array( $cat1, $cat2, $cat3 );
    
                foreach ( $cat_array as $term ) { 
                    $int_term = (int)$term;
                    $hypercommerce_woo_cat_name = get_term_by( 'id', $term, 'product_cat' );
                    $hypercommerce_cat_thumbnail_id = get_woocommerce_term_meta( $term, 'thumbnail_id' );
                    if( $hypercommerce_cat_thumbnail_id ){
                        $hypercommerce_cat_thumbnail_url =	 wp_get_attachment_image_src( $hypercommerce_cat_thumbnail_id, 'hypercommerce-featured-product-cat' );		
                    }else{
                        $hypercommerce_cat_thumbnail_url = false;
                    }
                if ($hypercommerce_cat_thumbnail_url) {	
                ?>
                  <div class="col-lg-4 col-sm-4">
                    <article class="post shop-card">
                        <img src="<?php echo esc_url($hypercommerce_cat_thumbnail_url[0]); ?>" class="img-responsive" alt="">
                        <div class="overlay"></div>
                        <div class="text-holder">
                            <h2>
                                <a href="<?php echo esc_url( get_term_link( $int_term ) ); ?>">                	
		                            <?php echo esc_html($hypercommerce_woo_cat_name->name); ?>
                                </a>
                            </h2>
                            <a href="<?php echo esc_url( get_term_link( $int_term ) ); ?>" class="btn shop cat-shop-btn"><?php echo esc_html( get_theme_mod( 'hypercommerce_woo_featured_button_setting', __('Shop', 'hyper-commerce') ) ); ?></a>
                            
                        </div>
                    </article>
                  </div>            
                <?php
                }
                    }
                ?>
              
</section>	

<div class="clearfix"></div>