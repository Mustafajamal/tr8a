<?php
/**
 * Template part for displaying latest products on custom homepage.
 *
 * @package hyper-commerce
 */
?>

					<section class="arrival">
			            <?php  $arrival_title = get_theme_mod( 'hypercommerce_woo_arrivals_title_setting', __('New Arrivals', 'hyper-commerce') );   ?>
                          <h2 class="section-title"><?php echo esc_html( $arrival_title ); ?></h2>
                          <div class="arrival-card-wrapper">

							<?php
           					$product_listing = get_theme_mod( 'hypercommerce_woo_arrivals_list_setting', 4 );
                            $count = 1;
                            $args = array(
                                'post_type' => 'product',
                                'posts_per_page' => $product_listing,
                                );
                            $loop = new WP_Query( $args );
                            if ( $loop->have_posts() ) {
                            while ( $loop->have_posts() ) : $loop->the_post();
                            global $product;
                            ?>                            

                                    <div class="col-lg-3 col-sm-4">
                                      <div class="card-wrapper">
	                                    <?php if ( has_post_thumbnail() ) { ?>
                                            <div class="img-holder">                                        
                                            <a href="<?php the_permalink(); ?>">
                                                <?php the_post_thumbnail('thumbnail', array('class' => 'img-responsive')); ?>
                                            </a>
                                            </div>                                                                                
                                        <?php } ?>
                                        <div class="text-holder">
                                          <h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
                                          <div class="arrival-price-mini">
                                              <?php echo wp_kses_post($product->get_price_html());?>
                                            </div>
                                        </div>
                                      </div>
                                    </div>
                  
						  <?php                  
                            if ($count % 4 == 0) {
                                echo "<div class='clearfix'></div>";
                            }
                            $count++;
                            endwhile;
                            } else {
                                echo esc_html__( 'No products found', 'hyper-commerce' );
                            }
                            wp_reset_postdata();
                          ?>         	                               
                            

                          </div>
                        </section>