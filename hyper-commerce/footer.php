<?php
/**
 * The footer for our theme
 *
 * This is the template that displays all of the <footer> section and everything up until </html>
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package hyper-commerce
 */

?>
<footer id="colophon" class="site-footer" role="conteninfo">
  <div class="holder">
    <div class="container">
      <div class="widget-area">
        <div class="row">
          <div class="col-lg-3 col-sm-3" style="width: 50% !important;">
            <?php dynamic_sidebar('footer-1'); ?>
          </div>

      <div class="col-lg-3 col-sm-3">
            <?php dynamic_sidebar('footer-2'); ?>
          </div>


          <div class="col-lg-3 col-sm-3">
            <?php dynamic_sidebar('footer-3'); ?>
          </div>

          <div class="col-lg-3 col-sm-3">
            <?php dynamic_sidebar('footer-4'); ?>
          </div>

        </div>
      </div>
    </div>
  </div>
  <div class="site-info">
	  <div class="copyright-notice">
		  <h6>
			  © 2018 TR8A.com ALL RIGHTS RESERVED
		  </h6>
	  </div>
	  <div class="legal-links">
		  <a href="http://tr8a.com/privacy-policy">Privacy Policy</a> | <a href="http://tr8a.com/terms">Terms & Conditions</a>
	  </div>
    <!--<?php do_action( 'hypercommerce_footer' )?>-->
  </div>  
</footer>
</div>

<?php wp_footer(); ?>       
</body>
</html>