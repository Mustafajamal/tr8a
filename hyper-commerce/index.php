<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package hyper-commerce
 */

get_header(); ?>

<?php  
  if( get_theme_mod( 'hypercommerce_breadcrumb_setting', 0 ) == 1 ){
?>
<div class="breadcrumbs">
  <div class="container">
    <div class="row">
      <div id="crumbs">
        <?php hypercommerce_get_breadcrumb(); ?>
      </div>
    </div>
  </div>
</div>
<?php } ?>

<div class="custom-header">
<center>
<img src="<?php header_image(); ?>" height="<?php echo esc_attr(get_custom_header()->height); ?>" width="<?php echo esc_attr(get_custom_header()->width); ?>" alt="" />
</center>
</div>


<!-- <div id="content" class="site-content">
  <div class="container">
    <div class="row"> -->
      <div id="content" class="site-content">
        <div class="container">
          <div class="row">
            <div class="col-lg-9 col-md-9">
              <div id="primary" class="content-area">
                <main id="main" class="site-main">
					<?php		
                      if( have_posts() ) :
            
                        if( is_home() && ! is_front_page() ) : ?>
            
                      <header>
                       <h1 class="page-title screen-reader-text"><?php single_post_title(); ?></h1>
                     </header>
            
                     <?php
                     endif;
            
                     /* Start the Loop */
                     while ( have_posts() ) : the_post();
                     get_template_part( 'template-parts/content' );
                     endwhile;
                     
                    $post_args =  array(
                        'screen_reader_text' => ' ',
                        'prev_text' => __( '<div class="chevronne"><i class="fa fa-chevron-left"></i></div>', 'hyper-commerce' ),
                        'next_text' => __( '<div class="chevronne"><i class="fa fa-chevron-right"></i></div>', 'hyper-commerce' ),
                        );
            
                    if( get_theme_mod( 'hypercommerce_pagination_setting', 'number') == 'text-pagination' ) { 				
                        the_posts_navigation();
                    }
                     else{        
                        the_posts_pagination( $post_args );        
                     }		                      
            
                     else :
                      get_template_part( 'template-parts/content', 'none' );
                    endif; 
				  ?>                  
                </main>
              </div>
            </div>

            <?php get_sidebar(); ?>
          </div>
        </div>
      </div>
    <!-- </div>
  </div>
</div> -->
</div>

<?php get_footer(); ?>