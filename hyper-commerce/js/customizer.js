/**
 * File customizer.js.
 *
 * Theme Customizer enhancements for a better user experience.
 *
 * Contains handlers to make Theme Customizer preview reload changes asynchronously.
 */

( function( $ ) {
	"use strict";

	// Site title and description.
	wp.customize( 'blogname', function( value ) {
		value.bind( function( to ) {
			$( '.site-branding .site-title a' ).text( to );
		} );
	} );
	wp.customize( 'blogdescription', function( value ) {
		value.bind( function( to ) {
			$( '.site-description' ).text( to );
		} );
	} );

	//Featured Posts title
	wp.customize( 'hypercommerce_woo_featured_p_title_setting', function( value ) {
		value.bind( function( to ) {
			jQuery( '.featured_product .section-title' ).text( to );
		} );
	} );

	//Cart Label
	wp.customize( 'hypercommerce_woo_mycart_label_setting', function( value ) {
		value.bind( function( to ) {
			jQuery( '.header-cart .btn' ).html( to + '<span class="caret"></span>' );
		} );
	} );

	//New Arrivals title
	wp.customize( 'hypercommerce_woo_arrivals_title_setting', function( value ) {
		value.bind( function( to ) {
			jQuery( '.arrival .section-title' ).text( to );
		} );
	} );
	

	//Show by cat title
	wp.customize( 'hypercommerce_woo_featured_title_setting', function( value ) {
		value.bind( function( to ) {
			jQuery( '.shop_category .section-title' ).text( to );
		} );
	} );

	wp.customize( 'hypercommerce_woo_featured_button_setting', function( value ) {
		value.bind( function( to ) {
			jQuery( '.cat-shop-btn' ).text( to );
		} );
	} );

	//slider button label
	wp.customize( 'hypercommerce_woo_slider_button_label', function( value ) {
		value.bind( function( to ) {
			jQuery( '.top-section .flexslider ul.slides li .caption a' ).text( to );
		} );
	} );

	// Header text color.
	wp.customize( 'header_textcolor', function( value ) {
		value.bind( function( to ) {
			if ( 'blank' === to ) {
				$( '.site-title, .site-description' ).css( {
					'clip': 'rect(1px, 1px, 1px, 1px)',
					'position': 'absolute'
				} );
			} else {
				$( '.site-title, .site-description' ).css( {
					'clip': 'auto',
					'position': 'relative'
				} );
				$( '.site-branding .site-title a, .site-description' ).css( {
					'color': to
				} );
			}
		} );
	} );
} )( jQuery );
