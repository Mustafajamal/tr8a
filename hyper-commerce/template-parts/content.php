<?php
/**
 * Template part for displaying posts
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package hyper-commerce
 */

?>
			<article id="post-<?php the_ID(); ?>" <?php post_class('post'); ?>>            
					<?php if ( has_post_thumbnail() ) { ?>                    
                        <div class="featured-img">
                            <a href="<?php the_permalink(); ?>" class="post-thumbnail">
                                <?php the_post_thumbnail('hypercommerce-featured', array('class' => 'img-responsive')); ?>
                            </a>
                        </div>  
					<?php } ?>                                                          
                    <div class="text-holder">
                      <header class="entry-header">
                        <h2 class="entry-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
                        <div class="entry-meta">
                          <span class="author"><?php esc_html_e('By', 'hyper-commerce'); ?> <?php the_author(); ?></span>
                          <span class="seperator"> <?php esc_html_e('-', 'hyper-commerce'); ?> </span>                          
                          <span class="time"><?php echo get_the_date(get_option( 'date-format' ) ); ?></span>
                          <span class="seperator"> <?php esc_html_e('-', 'hyper-commerce'); ?> </span>                          
                          <span class="category">
							<?php            
                                $categories = get_the_category();
                                if ( !empty( $categories ) ) { ?>
                                <span><a href="<?php echo esc_url(get_category_link( $categories[0]->term_id )); ?>"><?php echo esc_html( $categories[0]->name ); ?></a></span>
                            <?php } ?>                          
                          </span>
                        </div>
                      </header>
                      <div class="entry-excerpt">
						<?php the_excerpt(); ?>
                      </div>
                      <div class="entry-footer">
							<a href="<?php the_permalink(); ?>"><button class="btn read-more" type="button"><?php esc_html_e('Read more', 'hyper-commerce') ?></button></a>
	                  </div>
                    </div>
				</article><!-- #post-<?php the_ID(); ?> -->