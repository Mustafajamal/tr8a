<?php
/**
 * Template part for displaying pages
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package hyper-commerce
 */

?>

				<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>                                       
                    <div class="featured-img">
                        <?php
                        if ( has_post_thumbnail() ) {
                            the_post_thumbnail( 'hypercommerce-featured-single' );
                        }
                       ?>
                    </div>                                        
                    <header class="entry-header">
					  <?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>						                      
                    </header>
                    <div class="entry-content">
						<?php the_content(); 

                            wp_link_pages( array(
                                'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'hyper-commerce' ),
                                'after'  => '</div>',
                            ) );
                        ?>
                    </div>                                         
                  </article>