<?php
/**
 * Template part for displaying singles posts
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package hyper-commerce
 */

?>

				<article id="post-<?php the_ID(); ?>" <?php post_class('post'); ?>>                                       
                    <div class="featured-img">
                        <?php
                        if ( has_post_thumbnail() ) {
                            the_post_thumbnail( 'hypercommerce-featured-single' );
                        }
                       ?>
                    </div>                                        
                    <header class="entry-header">
					  <?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
						<div class="entry-meta">
                          <span class="author"><?php esc_html_e('By', 'hyper-commerce'); ?> <?php the_author(); ?></span>
                          <span class="seperator"> <?php esc_html_e('-', 'hyper-commerce'); ?> </span>                          
                          <span class="time"><?php echo get_the_date(get_option( 'date-format' ) ); ?></span>
                          <span class="seperator"> <?php esc_html_e('-', 'hyper-commerce'); ?> </span>                          
                          <span class="category">
							<?php            
                                $categories = get_the_category();
                                if ( !empty( $categories ) ) { ?>
                                <span><a href="<?php echo esc_url(get_category_link( $categories[0]->term_id )); ?>"><?php echo esc_html( $categories[0]->name ); ?></a></span>
                            <?php } ?>                          
                          </span>
                        </div>                      
                    </header>
                    <div class="entry-content">
                        <?php the_content(); 
                        wp_link_pages( array(
                            'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'hyper-commerce' ),
                            'after'  => '</div>',
                        ) );
                        ?>
                    </div>                     
                     
                    <?php if( has_tag() ) : ?>
                    <span class="posted-tags">
                        <?php the_tags( '<ul class="tags"><li><i class="fa fa-tags" aria-hidden="true"></i></li><li>', '</li><li>', '</li></ul>' ); ?>
                    </span>                
                    <?php endif; ?>                  
						<?php 
                            do_action( 'hypercommerce_author_bio' );
                                if ( comments_open() || get_comments_number() ) :
                                comments_template();
                                endif; 
                        ?>
                  </article>