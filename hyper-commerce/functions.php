<?php
/**
 * hypercommerce functions and definitions
 * 
 * Do not go gentle into that good night,
 * Old age should burn and rave at close of day;
 * Rage, rage against the dying of the light.
 * 
 * Though wise men at their end know dark is right,
 * Because their words had forked no lightning they
 * Do not go gentle into that good night.
 *  
 * Dylan Thomas, 1914 - 1953
 *   
 * This program is free software; you can redistribute it and/or modify it under the terms of the GNU
 * General Public License as published by the Free Software Foundation; either version 2 of the License,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 * You should have received a copy of the GNU General Public License along with this program; if not, write
 * to the Free Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA
 *
 * @package    hyper-commerce
 * @subpackage Functions
 * @author     ThemeThread <support@themethread.com>
 * @copyright  Copyright (c) 2017, ThemeThread
 * @link       http://themethread.com/theme/hyercommerce
 * @license    http://www.gnu.org/licenses/old-licenses/gpl-2.0.html
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package hypercommerce
 */

// Add Shortcode
function custom_shortcode() {

	// Code
	$user_id = get_the_author_meta( 'ID' );
	echo mycred_get_users_cred( $user_id );

}
add_shortcode( 'mycred_custom_shortcode', 'custom_shortcode' );

add_filter( 'mycred_parse_tags_user', 'my_custom_user_template_tags', 10, 3 );
function my_custom_user_template_tags( $content, $user, $data )
{
	$content = str_replace( '%avatar_small%', get_avatar( $user->ID, 64 ), $content );
	return $content; 
}

/**
 * Convert myCRED Points into WooCommerce Coupon
 * Requires myCRED 1.4 or higher!
 * @version 1.3.1
 */

/*
function prefix_run_on_competitions() {
    if( is_page( 119 )) {
		if(is_user_logged_in())
    	{
        	
		}
    }
}
add_action( 'wp_head', 'prefix_run_on_competitions' );
*/
define( 'SHOW_MYCRED_IN_WOOCOMMERCE', true );

add_filter( 'wp_nav_menu_items', 'user_name_menu_item');
function user_name_menu_item($items)
{
    if(is_user_logged_in())
    {
        $user=wp_get_current_user();
        $name=$user->user_firstname; // or user_login , user_firstname, user_lastname
        $items .= '<li><a class="custom-nav-item" href="http://tr8a.com/user/">Welcome '.$name.'</a></li>';
    }
    return $items;
}

add_filter( 'wp_nav_menu_items', 'role_menu_item');
function role_menu_item($items)
{
    if(is_user_logged_in())
    {
        $user = wp_get_current_user();
    	$role = ( array ) $user->roles; //array of roles the user is part of.
		if(sizeof($role) > 1) {
			$items .= '<li><a class="custom-nav-item" href="http://tr8a.com/account/">' .$role[1].'</a></li>';
		} else {
			$items .= '<li><a class="custom-nav-item" href="http://tr8a.com/account/">' .$role[0].'</a></li>';
		}
    }
    return $items;
}

function getUsersByRole($role,$name,$selected = '',$extra = '') {
global $wpdb;
 
	  $wp_user_search = new WP_User_Query(array("role"=> $role));
   	  $role_data = $wp_user_search->get_results();
		 foreach($role_data  as $item){
			 $role_data_ids[] = $item->ID;
		 }
 
	  $ids = implode(',', $role_data_ids);
	  $r = $wpdb->get_results("SELECT *   from ".$wpdb->prefix . "users where id IN(".$ids .")", ARRAY_A);
 
	  $content .='<select name="'.$name.'" id="'.$name.'" '.$extra.'>';
 
	  if($selected == ''){
			$content .='<option value="" selected="selected">Choose a user</option>';
		}else{
				$r_user = $wpdb->get_results("SELECT *   from ".$wpdb->prefix . "users where ID = ".$selected."", ARRAY_A);
				$content .='<option value="'.$selected.'" selected="selected">'.stripslashes($r_user[0]['user_login']).'</option>';
		}
 
	  for($i=0; $i<count($r); $i++){		  	
	  $content .='<option value="'.$r[$i]['ID'].'">'.stripslashes($r[$i]['user_login']).'</option>';
	  }
	  $content .='</select>';
 
	  return $content;
}

add_filter( 'woocommerce_checkout_fields' , 'custom_override_checkout_fields' );
 
function custom_override_checkout_fields( $fields ) {
    unset($fields['billing']['billing_first_name']);
    unset($fields['billing']['billing_last_name']);
    unset($fields['billing']['billing_company']);
    unset($fields['billing']['billing_address_1']);
    unset($fields['billing']['billing_address_2']);
    unset($fields['billing']['billing_city']);
    unset($fields['billing']['billing_postcode']);
    unset($fields['billing']['billing_country']);
    unset($fields['billing']['billing_state']);
    unset($fields['billing']['billing_phone']);
    unset($fields['order']['order_comments']);
    unset($fields['billing']['billing_address_2']);
    unset($fields['billing']['billing_postcode']);
    unset($fields['billing']['billing_company']);
    unset($fields['billing']['billing_last_name']);
    unset($fields['billing']['billing_email']);
    unset($fields['billing']['billing_city']);
    return $fields;
}

if ( ! function_exists( 'hypercommerce_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function hypercommerce_setup() {
		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on hypercommerce, use a find and replace
		 * to change 'hypercommerce' to the name of your theme in all the template files.
		 */
		load_theme_textdomain( 'hypercommerce', get_template_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );		
		add_image_size('hypercommerce-featured', 400, 280, true);		
		add_image_size('hypercommerce-featured-single', 860, 380, true);
		add_image_size('hypercommerce-featured-product-cat', 376, 251, true);		

		// This theme uses wp_nav_menu() in one location.
		register_nav_menus( array(
			'menu-1' => esc_html__( 'Primary', 'hyper-commerce' ),
		) );
		register_nav_menus( array(
			'top-menu' => esc_html__( 'Secondary', 'hyper-commerce' ),
		) );

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support( 'html5', array(
			'search-form',
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
		) );

		// Set up the WordPress core custom background feature.
		add_theme_support( 'custom-background', apply_filters( 'hypercommerce_custom_background_args', array(
			'default-color' => 'f1f1f1',
			'default-image' => '',
		) ) );

		// Add theme support for selective refresh for widgets.
		add_theme_support( 'customize-selective-refresh-widgets' );

		/**
		 * Add support for core custom logo.
		 *
		 * @link https://codex.wordpress.org/Theme_Logo
		 */
		add_theme_support( 'custom-logo', array(
			'height'      => 45,
			'width'       => 250,
			'flex-width'  => true,
			'flex-height' => true,
		) );
	}
endif;
add_action( 'after_setup_theme', 'hypercommerce_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function hypercommerce_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'hypercommerce_content_width', 640 );
}
add_action( 'after_setup_theme', 'hypercommerce_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function hypercommerce_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Main Sidebar', 'hyper-commerce' ),
		'id'            => 'sidebar-1',
		'description'   => esc_html__( 'Add widgets here.', 'hyper-commerce' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
	
	register_sidebar( array(
		'name'          => esc_html__( 'Homepage Widget', 'hyper-commerce' ),
		'id'            => 'homepage-aside',
		'description'   => esc_html__( 'Add widgets here.', 'hyper-commerce' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );	
	
	register_sidebar(array(
		'name'	=> esc_html__('Footer 1', 'hyper-commerce'),
		'id'	=> 'footer-1',
		'description'   => esc_html__( 'Add widgets here.', 'hyper-commerce' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	));
	register_sidebar(array(
		'name'	=> esc_html__('Footer 2', 'hyper-commerce'),
		'id'	=> 'footer-2',
		'description'   => esc_html__( 'Add widgets here.', 'hyper-commerce' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	));
	register_sidebar(array(
		'name'	=> esc_html__('Footer 3', 'hyper-commerce'),
		'id'	=> 'footer-3',
		'description'   => esc_html__( 'Add widgets here.', 'hyper-commerce' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	));
	register_sidebar(array(
		'name'	=> esc_html__('Footer 4', 'hyper-commerce'),
		'id'	=> 'footer-4',
		'description'   => esc_html__( 'Add widgets here.', 'hyper-commerce' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	));


	if(hypercommerce_woocommerce_activated()){
		register_sidebar(array(
			'name'	=> esc_html__('Woocommerce Sidebar', 'hyper-commerce'),
			'id'	=> 'sidebar-woocommerce',
			'description'   => esc_html__( 'Add widgets here.', 'hyper-commerce' ),
			'before_widget' => '<section id="%1$s" class="widget %2$s">',
			'after_widget'  => '</section>',
			'before_title'  => '<h2 class="widget-title">',
			'after_title'   => '</h2>',
		));
	}
}
add_action( 'widgets_init', 'hypercommerce_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function hypercommerce_scripts() {

	/*enqueue custon style*/
	wp_enqueue_style('bootstrap', get_template_directory_uri(). '/css/bootstrap.css');	
	wp_enqueue_style('flexslider', get_template_directory_uri(). '/css/flexslider.css');	
	wp_enqueue_style('font-awesome', get_template_directory_uri(). '/css/font-awesome.css');
	wp_enqueue_style('jquery-smartmenus', get_template_directory_uri(). '/css/jquery.smartmenus.bootstrap.css');	
	
	if( hypercommerce_woocommerce_activated() ){
		wp_enqueue_style('hypercommerce-woo-styles', get_template_directory_uri(). '/css/woo-style.css');		
	}
	
	wp_enqueue_style( 'hypercommerce-style', get_stylesheet_uri() );	

	/*enqueue custom js*/
	wp_enqueue_script('jquery-bootstrap', get_template_directory_uri().'/js/bootstrap.js',array('jquery'),'20151215',true);
	wp_enqueue_script('jquery-smartmenus',get_template_directory_uri().'/js/jquery.smartmenus.js', array('jquery'), '20151215', true);
	wp_enqueue_script('jquery-flexslider', get_template_directory_uri().'/js/jquery.flexslider.js', array('jquery'), '20151215', true);
	wp_enqueue_script('jquery-slimscroll', get_template_directory_uri().'/js/jquery.slimscroll.js', array('jquery'), '20151215', true);
	wp_enqueue_script('jquery-matchheight', get_template_directory_uri().'/js/jquery.matchHeight.js', array('jquery'), '20151215', true);
	wp_enqueue_script('jquery-smartmenus-bootstrap', get_template_directory_uri().'/js/jquery.smartmenus.bootstrap.js', array('jquery'), '20151215', true);		
	wp_enqueue_script('hypercommerce-custom', get_template_directory_uri().'/js/custom.js', array('jquery'), '20151215', true);	

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'hypercommerce_scripts' );

/**
 * Admin css
 */
function hypercommerce_admin_scripts() {

	wp_enqueue_style( 'hypercommerce-admin-style',get_template_directory_uri().'/inc/css/admin.css', '1.0', 'screen' );
	wp_enqueue_script( 'hypercommerce-admin-js', get_template_directory_uri().'/inc/js/admin.js', array( 'jquery' ), '', true );
}
add_action( 'customize_controls_enqueue_scripts', 'hypercommerce_admin_scripts' );

/**
 * Register custom fonts.
 * Also to choose fonts from theme customizer for title. */
function hypercommerce_header_font_url() {
	$fonts_header_url = '';
	$hypercommerce_header_font = get_theme_mod('hypercommerce_header_font_setting', 'opensans');
	if($hypercommerce_header_font == 'firasans'){
			$fira = _x('on', 'Fira Sans : on or off', 'hyper-commerce');
	}
	else{
			$fira = _x('off', 'Fira Sans : on or off', 'hyper-commerce');	
	}
	if($hypercommerce_header_font == 'lora'){
			$lora = _x('on', 'Lora : on or off', 'hyper-commerce');
	}
	else{
			$lora = _x('off', 'Lora : on or off', 'hyper-commerce');	
	}
	if($hypercommerce_header_font == 'poiretone'){
			$poiretone = _x('on', 'Poiret One : on or off', 'hyper-commerce');
	}
	else
	{
	$poiretone = _x('off', 'Poiret One : on or off', 'hyper-commerce'); 
	}
	if($hypercommerce_header_font == 'opensans'){
			$opensans = _x( 'on', 'Open Sans: on or off', 'hyper-commerce' );	}
	else{
			$opensans = _x( 'off', 'Open Sans: on or off', 'hyper-commerce' );	
	}
	if($hypercommerce_header_font == 'roboto'){
			$roboto = _x('on', 'Roboto : on or off', 'hyper-commerce');	}
	else{
			$roboto = _x('off', 'Roboto : on or off', 'hyper-commerce');	
	}
	if($hypercommerce_header_font == 'ubuntu'){
			$ubuntu = _x('on', 'Ubuntu : on or off', 'hyper-commerce');
	}
	else{
			$ubuntu = _x('off', 'Ubuntu : onn or off', 'hyper-commerce');
	}
	if ( 'off' !== $lora || 'off' !== $opensans || 'off' !== $roboto  || 'off' !== $poiretone  || 'off' !== $ubuntu || 'off' !== $fira ) {
	$font_families = array();	
	 
		if ( 'on' == $lora ) {
		$font_families[] = 'Lora';
		}
		 
		if ( 'on' == $opensans ) {
		$font_families[] = 'Open Sans:300,400,600,700,900';
		}
	
		if ( 'on' == $roboto) {
		$font_families[] = 'Roboto';
		}
	
		if('on' == $poiretone){
			$font_families[] = 'Poiret One';
		}
		
		if('on' == $ubuntu){
			$font_families[] = 'Ubuntu';
		}	

		if('on' == $fira){
			$font_families[] = 'Fira Sans:300,400,500,600,700';
		}
	 
		$query_args = array(
			'family' => urlencode( implode( '|', $font_families ) ),
		);
		 
		$fonts_header_url = add_query_arg( $query_args, 'https://fonts.googleapis.com/css' );
	}
	 
	return esc_url_raw( $fonts_header_url );

}
/**
 * Register custom fonts.
 * Also to choose fonts from theme customizer for body. */


function hypercommerce_body_font_url() {
	$fonts_body_url = '';
	$hypercommerce_body_font = get_theme_mod('hypercommerce_body_font_setting', 'firasans');
	if($hypercommerce_body_font == 'firasans'){
			$fira = _x('on', 'Fira Sans : on or off', 'hyper-commerce');
	}
	else{
			$fira = _x('off', 'Fira Sans : on or off', 'hyper-commerce');	
	}
	if($hypercommerce_body_font == 'lora'){
			$lora = _x('on', 'Lora : on or off', 'hyper-commerce');
	}
	else{
			$lora = _x('off', 'Lora : on or off', 'hyper-commerce');	
	}
	if($hypercommerce_body_font == 'poiretone'){
			$poiretone = _x('on', 'Poiret One : on or off', 'hyper-commerce');
	}
	else
	{
	$poiretone = _x('off', 'Poiret One : on or off', 'hyper-commerce'); 
	}
	if($hypercommerce_body_font == 'opensans'){
			$opensans = _x( 'on', 'Open Sans: on or off', 'hyper-commerce' );	}
	else{
			$opensans = _x( 'off', 'Open Sans: on or off', 'hyper-commerce' );	
	}
	if($hypercommerce_body_font == 'roboto'){
			$roboto = _x('on', 'Roboto : on or off', 'hyper-commerce');	}
	else{
			$roboto = _x('off', 'Roboto : on or off', 'hyper-commerce');	
	}
	if($hypercommerce_body_font == 'ubuntu'){
			$ubuntu = _x('on', 'Ubuntu : on or off', 'hyper-commerce');
	}
	else{
			$ubuntu = _x('off', 'Ubuntu : onn or off', 'hyper-commerce');
	}
	if ( 'off' !== $lora || 'off' !== $opensans || 'off' !== $roboto  || 'off' !== $poiretone  || 'off' !== $ubuntu || 'off' !== $fira ) {
	$font_families = array();	
	 
		if ( 'on' == $lora ) {
		$font_families[] = 'Lora';
		}
		 
		if ( 'on' == $opensans ) {
		$font_families[] = 'Open Sans:300,400,600,700,900';
		}
	
		if ( 'on' == $roboto) {
		$font_families[] = 'Roboto';
		}
	
		if('on' == $poiretone){
			$font_families[] = 'Poiret One';
		}
		
		if('on' == $ubuntu){
			$font_families[] = 'Ubuntu';
		}	

		if('on' == $fira){
			$font_families[] = 'Fira Sans:300,400,500,600,700';
		}
	 
		$query_args = array(
			'family' => urlencode( implode( '|', $font_families ) ),
		);
		 
		$fonts_body_url = add_query_arg( $query_args, 'https://fonts.googleapis.com/css' );
	}
	 
	return esc_url_raw( $fonts_body_url );

}
function hypercommerce_google_fonts() {
	$hypercommerce_header_font= hypercommerce_header_font_url();
	$hypercommerce_body_font = hypercommerce_body_font_url();
	
	if($hypercommerce_header_font == $hypercommerce_body_font){
		wp_enqueue_style( 'hypercommerce-fonts', $hypercommerce_body_font, array(), null );
	}else{
		wp_enqueue_style( 'hypercommerce-header-fonts', $hypercommerce_header_font, array(), null );
		wp_enqueue_style( 'hypercommerce-body-fonts', $hypercommerce_body_font, array(), null );
	}
}
add_action( 'wp_enqueue_scripts', 'hypercommerce_google_fonts' );

//change search form placeholder text

/**
 * Woo Commerce Sections
 */
if( class_exists('woocommerce') ){
	require get_template_directory() . '/inc/woo-functions.php';
	require get_template_directory() . '/inc/woo-customizer.php';
}

/**
 * Implement extra.php
 */
require get_template_directory().'/inc/extra.php';
/**
 * Implement the custom stylesheet
 */
require get_template_directory() . '/inc/style.php';
/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Navwalker for bootstrap
 */
require get_template_directory() . '/inc/wp-bootstrap-navwalker.php';

/**
 * Theme Info in Customizer
 */
// require get_template_directory() . '/inc/info.php';

/**
 * Load Jetpack compatibility file.
 */
if ( defined( 'JETPACK__VERSION' ) ) {
	require get_template_directory() . '/inc/jetpack.php';
}

