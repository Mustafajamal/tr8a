=== Hyper Commerce ===

Author: ThemeThread (http://themethread.com)

Tags: two-columns, one-column, right-sidebar, featured-images, footer-widgets, full-width-template, threaded-comments, custom-logo, translation-ready, blog, e-commerce, food-and-drink, custom-header, custom-background, custom-colors, theme-options, custom-menu, flexible-header

Requires at least: 4.7
Tested up to: 4.9.1
Stable tag: 1.0.9
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

== Description ==
HyperCommerce is a fully responsive, WooCommerce supported, mobile first WordPress Theme. HyperCommerce provides a fast and easy way to build your dream e-shop, online store, e-commerce site or an online shop portal. With plenty of custom color, font, header, titles and buttons customization you can give this theme a new look to match your company�s branding. From product details page to checkout page, all the required pages for an e-commerce platform is well bundled. Download, customize, launch and start selling. Get in touch with the support for any kind of issues, we are here to make your e-commerce portal work like a charm. 

Theme demo: http://demo.themethread.com/hypercommerce

== Installation ==

1. In your admin panel, go to Appearance > Themes and click the Add New button.
2. Click Upload and Choose File, then select the theme's .zip file. Click Install Now.
3. Click Activate to use your new theme right away.

== Copyrights and License ==

Unless otherwise specified, all the theme files, scripts and images are licensed under GPLv2 or later

==  Credits ==

External resources linked to the theme.
* Open Sans by Steve Matteson through Google Font - Apache License, Version 2.0
https://fonts.google.com/specimen/Open+Sans

* Roboto Font by Christian Robertson through Google Font - Apache License, 2.0
https://fonts.google.com/specimen/Roboto

* Ubuntu Font by Dalton Maag through Google Font - Ubuntu Font License, 1.0
https://fonts.google.com/specimen/Ubuntu

* Lora by Cyreal through Google Font - SIL Open Font License, Version 2.0
https://fonts.google.com/specimen/Lora

* Poiret One by Denis Masharov through Google Font - SIL Open Font License, 1.1
https://fonts.google.com/specimen/Poiret+One

* Fira Sans One by Carrois Apostrophe through Google Font - SIL Open Font License, Version 2.0
https://fonts.google.com/specimen/Fira+Sans

* Font Awesome
http://fontawesome.github.io/Font-Awesome/license/

	**Font License
	Applies to all desktop and webfont files in the following directory: hyper-commerce/fonts/.
	License: SIL OFL 1.1
	URL: http://scripts.sil.org/OFL
	
	**Code License
	Applies to all CSS in the following file: hyper-commerce/css/font-awesome.css.
	License: MIT License
	URL: http://opensource.org/licenses/mit-license.html

* Underscores
http://underscores.me/
(C) 2012-2017 Automattic, Inc.
License: [GPLv2 or later]
URL: https://www.gnu.org/licenses/gpl-2.0.html

* FlexSlider
http://www.woocommerce.com/flexslider/ - 
Copyright (c) 2015 WooThemes
License: GNU General Public License v2.0
URL: https://www.gnu.org/licenses/gpl-2.0.html

* Twitter Bootstrap
http://getbootstrap.com/
(C) 2011-2017 Mark Otto and Jacob Thornton
License: MIT License and GPL (Dual License)
URL: https://github.com/twbs/bootstrap/blob/master/LICENSE

* SmartMenus
https://github.com/vadikom/smartmenus
License: MIT License
URL: http://opensource.org/licenses/mit-license.html

* slimScroll
https://github.com/rochal/jQuery-slimScroll
License: MIT License and GPL (Dual License)
URL: http://opensource.org/licenses/mit-license.html
URL: http://www.opensource.org/licenses/gpl-license.php

* MatchHeight
Copyright (c) 2014 liabru
http://brm.io/jquery-match-height/
License: MIT License
URL: http://opensource.org/licenses/MIT

* WP Bootstrap Navwalker
Copyright Edward McIntyre - @twittem, WP Bootstrap
https://github.com/wp-bootstrap/wp-bootstrap-navwalker
License: GPL-3.0+
License URI: http://www.gnu.org/licenses/gpl-3.0.txt

* Woocommerce
http://www.woocommerce.com/
License: GNU General Public License v3.0
URL: https://www.gnu.org/licenses/gpl-3.0.html

== Images == 
https://www.pexels.com/photo/adult-beanie-beard-coat-375880/ Public Domain CC0 License(https://www.pexels.com/photo-license/)
https://pixabay.com/en/smartphone-app-facebook-2271722/ CC0 Creative Commons (https://pixabay.com/en/service/terms/#usage)
https://pixabay.com/en/blue-denim-jacket-clothing-fashion-2564660/ CC0 Creative Commons (https://pixabay.com/en/service/terms/#usage)
https://pixabay.com/en/mobile-phone-iphone-music-616012/ CC0 Creative Commons (https://pixabay.com/en/service/terms/#usage)

== Changelog ==
1.0.0 = Initital Release
1.0.1 = Release with new colors and layout
1.0.2 = Added custom homepage
1.0.3 = Changed Woocommerce Panel in Customizer
1.0.4 = Fixed Navmenu, 	Added responsive css to image in textwidget, if featured image is not present then content is full width, post markup fixed
1.0.5 = Fixed theme review issues, prefixed functions.
1.0.6 = Added Copyright Text in footer. Fixed WP nav menu bootstrap walker, changed theme description
1.0.7 = Added color customization option for footer. Fixed price bug in new arrival section. 
1.0.8 = Added Text change option for Slider, Fixed font color issue for text widget, fixed layout issue for related products in mobile view.
1.0.9 = Added Pro Version Panel in Customizer


All other resources and theme elements are licensed under the GPLv2 or later

HyperCommerce WordPress Theme, Copyright ThemeThread 2017, ThemeThread.com
HyperCommerce WordPress Theme is distributed under the terms of the GPLv2 or later

*  This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
