<?php
/**
 * The template for displaying all single pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package hyper-commerce
 */

get_header(); 
?>
<?php  
  if( get_theme_mod( 'hypercommerce_breadcrumb_setting', 0 ) == 1 ){
?>
<div class="breadcrumbs">
  <div class="container">
    <div class="row">
      <div id="crumbs">
        <?php hypercommerce_get_breadcrumb(); ?>
      </div>
    </div>
  </div>
</div>
<?php } ?>


<!-- <div id="content" class="site-content">
  <div class="container">
    <div class="row"> -->
      <div id="content" class="site-content">
        <div class="container">
          <div class="row">
            
            <?php  
            $col_class = 'col-lg-9 col-md-9';

            if( (hypercommerce_woocommerce_activated() && (is_cart() || is_checkout())) && !is_active_sidebar( 'sidebar-woocommerce' ) ){
              $col_class = 'col-lg-12 col-md-12';
            }
            ?>

            <div class="<?php echo esc_attr($col_class); ?>">
              <div id="primary" class="content-area">
                <main id="main" class="site-main">
                  
					<?php
					while ( have_posts() ) : the_post(); //main loop

						get_template_part( 'template-parts/content', 'page' );

            		endwhile; // End of the loop.
					?>                  
                  
                </main>
              </div>
            </div>

            <?php 
            get_sidebar(); 
            ?>            
          </div>
        </div>
      </div>
    <!-- </div>
  </div>
</div> -->
</div>

<?php get_footer(); ?>