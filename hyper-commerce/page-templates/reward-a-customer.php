<?php
/**
 * Template Name: reward-a-customer
 *
 * Homepage layout for hyper-commerce. Uses mainly of woocommerce elements hence it required woocommerce to be 
 * activated
 *
 * @package hyper-commerce
 */

get_header(); ?>

<div class="main-section">

<?php 
  
  if( hypercommerce_woocommerce_activated() ) :

  if( get_theme_mod( 'hypercommerce_woo_slider_setting', 1 ) == 1 ){
   //  get_template_part( 'sections/home', 'slider' );     
  } ?>

<div class="clearfix"></div>

<div id="content" class="site-content">
    <div class="container">
      <div class="row">
        <div class="content-area">
          <main id="main" class="site-main" role="main">
			  <h1>
				   REWARD A CUSTOMER
			  </h1>
			  <h4>
				   Select a customer from the dropdown then input the amount and click 'REWARD CUSTOMER'
			  </h4>
          <form method="post">
          <?php echo getUsersByRole('um_subscriber','user_id'); ?>
          	<input type="text" name="rewardAmount" id="rewardAmount" value="" />
          	<input type="submit" name="rewardCustomer" id="rewardCustomer" value="REWARD CUSTOMER" />
          	<input type="hidden" name="selectedUser" id="user_hidden">
          </form>
			  
			  <script>
				  $(document).ready(function() {
					  $("#user_id").change(function(){
						  $("#user_hidden").val(("#user_id").find(":selected").text());
					  });
				  });
			  </script>
			  
			  
            <?php
            
            $amount = $_POST['rewardAmount'];
			$subscriber = $_POST['user_id'];

		function rewardAmount() {
            global $amount;
			global $subscriber;
			
			if( current_user_can('um_primark-staff')) {
				echo "<script type='text/javascript'>alert('hey');</script>";
            		echo do_shortcode("[mycred_give user_id=$subscriber type='p_lp' amount=$amount log='Bonus points for reading our special article' ref='bonus_points']");
			} elseif ( current_user_can('um_pets-at-home-staff')) {
				//	echo "<script type='text/javascript'>alert('$subscriber');</script>";
            		echo do_shortcode("[mycred_give user_id='$subscriber' type='pah_lp' amount=$amount log='Bonus points for reading our special article' ref='bonus_points']");
			} elseif ( current_user_can('um_apple-staff')) {
            		echo do_shortcode("[mycred_give user_id='$subscriber' type='a_lp' amount=$amount log='Bonus points for reading our special article' ref='bonus_points']");
			} else {
				echo "<script type='text/javascript'>alert('Sorry you do not have permission to reward these points');</script>";
				
			}
					
            }
            	
            if(array_key_exists('rewardAmount',$_POST)){
   				rewardAmount();
			}
            
            ?>
			  

                        
        </main>
       </div>
     </div>
  </div>
</div>

<div class="clearfix"></div>
<?php  
  endif;
?>
<?php get_footer(); ?>