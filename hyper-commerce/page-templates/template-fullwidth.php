<?php
/**
 * Template Name: Full Width
 *
 * @package hyper-commerce
 */

get_header(); 
?>
<?php  
  if( get_theme_mod( 'hypercommerce_breadcrumb_setting', 0 ) == 1 ){
?>
<div class="breadcrumbs">
  <div class="container">
    <div class="row">
      <div id="crumbs">
        <?php hypercommerce_get_breadcrumb(); ?>
      </div>
    </div>
  </div>
</div>
<?php } ?>



<div id="content" class="site-content">
  <div class="container">
    <div class="row">
      <div id="content" class="site-content">
        <div class="container">
          <div class="row">
            <div class="col-lg-8 col-md-8 col-lg-offset-2 col-md-offset-2 col-sm-12 col-xs-12">
              <div id="primary" class="content-area">
                <main id="main" class="site-main">
                  
					<?php
					while ( have_posts() ) : the_post(); //main loop

						get_template_part( 'template-parts/content', 'page' );

            		endwhile; // End of the loop.
					?>                  
                  
                </main>
              </div>
            </div>
      
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</div>

<?php get_footer(); ?>