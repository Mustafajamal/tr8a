<?php
/**
 * Template Name: Homepage
 *
 * Homepage layout for hyper-commerce. Uses mainly of woocommerce elements hence it required woocommerce to be 
 * activated
 *
 * @package hyper-commerce
 */

get_header(); ?>

<div class="main-section">

<?php 
  
  if( hypercommerce_woocommerce_activated() ) :

  if( get_theme_mod( 'hypercommerce_woo_slider_setting', 1 ) == 1 ){
     get_template_part( 'sections/home', 'slider' );     
  } ?>


<div class="clearfix"></div>

<div id="content" class="site-content">
    <div class="container">
      <div class="row">
        <div class="content-area">
          <main id="main" class="site-main" role="main">

            <?php 

            if( get_theme_mod( 'hypercommerce_woo_featured_setting', 1 ) == 1){
              get_template_part( 'sections/home', 'category' );   
            }

            if( get_theme_mod( 'hypercommerce_woo_featured_p_setting', 1 ) == 1){
              get_template_part( 'sections/home', 'featured' );   
            }

            if( get_theme_mod( 'hypercommerce_woo_arrivals_setting', 1 ) == 1 ){
              get_template_part( 'sections/home', 'latest' );       
            }
			
			?>
                        
        </main>
       </div>
     </div>
  </div>
</div>

<div class="clearfix"></div>
<?php  
  endif;
?>
<?php get_footer(); ?>