<?php 
/*to display comment box in bottom*/
function hypercommerce_move_comment_field_to_bottom( $fields ) {
$comment_field = $fields['comment'];
unset( $fields['comment'] );
$fields['comment'] = $comment_field;
return $fields;
}
add_filter( 'comment_form_fields', 'hypercommerce_move_comment_field_to_bottom' );


/**
 * Check woo commerce
 */
function hypercommerce_woocommerce_activated() {
  return class_exists( 'woocommerce' ) ? true : false;
}


if( ! function_exists( 'hypercommerce_hextorgba') ) :
function hypercommerce_hextorgba($color, $opacity = false) {
    $default = 'rgb(0,0,0)';    
    
    if (empty($color))
        return $default;    

    if ($color[0] == '#')
        $color = substr($color, 1);
    
    if (strlen($color) == 6)
        $hex = array($color[0] . $color[1], $color[2] . $color[3], $color[4] . $color[5]);
    
    elseif (strlen($color) == 3)
        $hex = array($color[0] . $color[0], $color[1] . $color[1], $color[2] . $color[2]);
    
    else
        return $default;
       
    $rgb = array_map('hexdec', $hex);    

    if ($opacity) {
        if (abs($opacity) > 1)
            $opacity = 1.0;

        $output = 'rgba(' . implode(",", $rgb) . ',' . $opacity . ')';
    } else {
        $output = 'rgb(' . implode(",", $rgb) . ')';
    }    
    return $output;
}
endif;

/**
 * Footer Credits
*/
	if ( ! function_exists( 'hypercommerce_footer_credit' ) ) :

		function hypercommerce_footer_credit(){
			$copyright = esc_html__( 'Copyright &copy; ', 'hyper-commerce' ).get_bloginfo( 'name' ).' '.date_i18n('Y');
			$text  = '<div class="container bottom-wrapper"><div class="col-md-4 no-padding"><h5 class="copyright">';
			$text .= $copyright;
			$text .= '</h5></div><div class="col-md-5 pull-right"><h5 class="powered">'; 
			$text .=  esc_html__( 'Theme Hyper Commerce by ', 'hyper-commerce' );        
			$text .= '<a href="' . esc_url( 'http://themethread.com/theme/hypercommerce' ) .'" rel="author" target="_blank">' . esc_html__( 'ThemeThread', 'hyper-commerce' ) .'</a> | ';
			/* translators: %s: wordpress.org URL */ 
			$text .= sprintf( esc_html__( 'Powered By %s', 'hyper-commerce' ), '<a href="'. esc_url( __( 'https://wordpress.org/', 'hyper-commerce' ) ) .'" target="_blank">WordPress</a>.' ); 
			$text .= '</h5></div></div>';
		
			echo apply_filters( 'hypercommerce_footer_text', $text ); // WPCS: xss ok
		}
    
    add_action( 'hypercommerce_footer', 'hypercommerce_footer_credit' );
    
    endif;