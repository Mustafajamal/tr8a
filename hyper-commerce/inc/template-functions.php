<?php
/**
 * Functions which enhance the theme by hooking into WordPress
 *
 * @package hypercommerce
 */

/**
 * Adds custom classes to the array of body classes.
 *
 * @param array $classes Classes for the body element.
 * @return array
 */
function hypercommerce_body_classes( $classes ) {
	// Adds a class of hfeed to non-singular pages.
	if ( ! is_singular() ) {
		$classes[] = 'hfeed';
	}
	
	if ( is_archive() ) {
		$classes[] = 'blog';
	}	

	return $classes;
}
add_filter( 'body_class', 'hypercommerce_body_classes' );

/**
 * Add a pingback url auto-discovery header for singularly identifiable articles.
 */
function hypercommerce_pingback_header() {
	if ( is_singular() && pings_open() ) {
		echo '<link rel="pingback" href="', esc_url( get_bloginfo( 'pingback_url' ) ), '">';
	}
}
add_action( 'wp_head', 'hypercommerce_pingback_header' );



if( ! function_exists( 'hypercommerce_author_bio_cb' ) ) :
/**
 * Author Bio 
*/
function hypercommerce_author_bio_cb(){ 
    if( get_the_author_meta( 'description' ) ){ ?>
					<div class="author-section">
						  <div class="img-holder">
							<?php echo get_avatar( get_the_author_meta( 'ID' ), 190, '', '', array( 'class' => array('img-responsive') ) ); ?>
						  </div>
						  <div class="text-holder">
							<h2 class="author-header"><?php echo get_the_author(); ?></h2>
						  <?php echo wp_kses_post( get_the_author_meta( 'description' ) ); ?>
						  </div>
                    </div>
					<div class="clearfix"></div>        
        
    <?php
    }
}
endif;
add_action( 'hypercommerce_author_bio', 'hypercommerce_author_bio_cb' );