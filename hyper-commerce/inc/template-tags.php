<?php
/**
 * Custom template tags for this theme
 *
 * Eventually, some of the functionality here could be replaced by core features.
 *
 * @package hypercommerce
 */

if ( ! function_exists( 'hypercommerce_posted_on' ) ) :
	/**
	 * Prints HTML with meta information for the current post-date/time and author.
	 */
	function hypercommerce_posted_on() {
		$time_string = '<time class="entry-date published updated" datetime="%1$s">%2$s</time>';
		if ( get_the_time( 'U' ) !== get_the_modified_time( 'U' ) ) {
			$time_string = '<time class="entry-date published" datetime="%1$s">%2$s</time><time class="updated" datetime="%3$s">%4$s</time>';
		}

		$time_string = sprintf( $time_string,
			esc_attr( get_the_date( 'c' ) ),
			esc_html( get_the_date() ),
			esc_attr( get_the_modified_date( 'c' ) ),
			esc_html( get_the_modified_date() )
		);

		$posted_on = sprintf(
			/* translators: %s: post date. */
			esc_html_x( 'Posted on %s', 'post date', 'hyper-commerce' ),
			'<a href="' . esc_url( get_permalink() ) . '" rel="bookmark">' . $time_string . '</a>'
		);

		$byline = sprintf(
			/* translators: %s: post author. */
			esc_html_x( 'by %s', 'post author', 'hyper-commerce' ),
			'<span class="author vcard"><a class="url fn n" href="' . esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ) . '">' . esc_html( get_the_author() ) . '</a></span>'
		);

		echo '<span class="posted-on">' . $posted_on . '</span><span class="byline"> ' . $byline . '</span>'; // WPCS: XSS OK.

	}
endif;

if ( ! function_exists( 'hypercommerce_entry_footer' ) ) :
	/**
	 * Prints HTML with meta information for the categories, tags and comments.
	 */
	function hypercommerce_entry_footer() {
		// Hide category and tag text for pages.
		if ( 'post' === get_post_type() ) {
			/* translators: used between list items, there is a space after the comma */
			$categories_list = get_the_category_list( esc_html__( ', ', 'hyper-commerce' ) );
			if ( $categories_list ) {
				/* translators: 1: list of categories. */
				printf( '<span class="cat-links">' . esc_html__( 'Posted in %1$s', 'hyper-commerce' ) . '</span>', $categories_list ); // WPCS: XSS OK.
			}

			/* translators: used between list items, there is a space after the comma */
			$tags_list = get_the_tag_list( '', esc_html_x( ', ', 'list item separator', 'hyper-commerce' ) );
			if ( $tags_list ) {
				/* translators: 1: list of tags. */
				printf( '<span class="tags-links">' . esc_html__( 'Tagged %1$s', 'hyper-commerce' ) . '</span>', $tags_list ); // WPCS: XSS OK.
			}
		}

		if ( ! is_single() && ! post_password_required() && ( comments_open() || get_comments_number() ) ) {
			echo '<span class="comments-link">';
			comments_popup_link(
				sprintf(
					wp_kses(
						/* translators: %s: post title */
						__( 'Leave a Comment<span class="screen-reader-text"> on %s</span>', 'hyper-commerce' ),
						array(
							'span' => array(
								'class' => array(),
							),
						)
					),
					get_the_title()
				)
			);
			echo '</span>';
		}

		edit_post_link(
			sprintf(
				wp_kses(
					/* translators: %s: Name of current post. Only visible to screen readers */
					__( 'Edit <span class="screen-reader-text">%s</span>', 'hyper-commerce' ),
					array(
						'span' => array(
							'class' => array(),
						),
					)
				),
				get_the_title()
			),
			'<span class="edit-link">',
			'</span>'
		);
	}
endif;

//Bread Crumb
if( ! function_exists( 'hypercommerce_get_breadcrumb' ) ) :
function hypercommerce_get_breadcrumb() {
	printf( '<a href="%s" class="link">%s</a>', esc_url( home_url() ), esc_html__( 'Home', 'hyper-commerce' ) );
    if (is_category() || is_single()) {
        // echo "&nbsp;&nbsp;&#187;&nbsp;&nbsp;";
    		echo esc_attr__( "&nbsp;-&nbsp; ", "hyper-commerce" );
        the_category(' &bull; ');
            if (is_single()) {
            		if( hypercommerce_woocommerce_activated() && is_woocommerce() ){
            			$woo_cat = get_the_terms( get_the_ID(), 'product_cat');
            			$main_count = 0;
            			foreach ($woo_cat as $wcat) {
            				$main_count ++;
            				echo '<a href="'.esc_url(get_term_link($wcat->term_id)).'">'.esc_html($wcat->name).'</a>';
            				if( $main_count != count($woo_cat) ){
            					echo esc_attr(' &bull; ');
            				}
            			}
            		}
            		echo esc_attr__( "&nbsp;-&nbsp; ", "hyper-commerce" );
                the_title();
            }
    } elseif (is_page()) {
    	echo esc_attr__( "&nbsp;-&nbsp; ", "hyper-commerce" );
      the_title();
    } elseif (is_search()) {
        echo esc_attr__( "&nbsp;-&nbsp;Search Results for... ", "hyper-commerce" );
        echo '"<em>';
        echo the_search_query();
        echo '</em>"';
    } elseif (is_archive()){
    	echo esc_attr__( "&nbsp;-&nbsp; ", "hyper-commerce" );
    	the_archive_title( '', '' );
    }
}
// echo '</div></div></div></div>';
endif;
