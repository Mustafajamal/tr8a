<?php
/**
 * HyperCommerce Theme Customizer
 *
 * @package HyperCommerce
 */

/**
 * Add postMessage support for site title and description for the Theme Customizer.
 *
 * @param WP_Customize_Manager $wp_customize Theme Customizer object.
 */
function hypercommerce_customize_register( $wp_customize ) {
	$wp_customize->get_setting( 'blogname' )->transport         = 'postMessage';
	$wp_customize->get_setting( 'blogdescription' )->transport  = 'postMessage';
	$wp_customize->get_setting( 'header_textcolor' )->transport = 'postMessage';

	if ( isset( $wp_customize->selective_refresh ) ) {
		$wp_customize->selective_refresh->add_partial( 'blogname', array(
			'selector'        => '.site-branding .site-title a',
			'render_callback' => 'hypercommerce_customize_partial_blogname',
		) );
		$wp_customize->selective_refresh->add_partial( 'blogdescription', array(
			'selector'        => '.site-description',
			'render_callback' => 'hypercommerce_customize_partial_blogdescription',
		) );
	}

	/**
	 * custom customize
	 */

	//Move colors section to custom panel
	$wp_customize->get_section ('colors')->panel = 'hypercommerce_general_panel';
	$wp_customize->get_section ('static_front_page')->title = __('Default Settings', 'hyper-commerce');
	$wp_customize->get_section ('static_front_page')->panel = 'hypercommerce_homepage_panel';


	$wp_customize->add_panel('hypercommerce_homepage_panel',
		array(
			'title'	=>	__('Homepage Settings', 'hyper-commerce'),
			'priority'	=>	99,
		)
	);

	/**
	 * primary and secondary color
	 */

	$wp_customize->add_setting('hypercommerce_primary_color_setting',
		array(
			'default' 		=> '#FF4B59',
			'transport'		=> 'refresh',
			'sanitize_callback'	=>'hypercommerce_sanitize_hex_color',
		)
	);
	$wp_customize->add_control(new WP_Customize_Color_Control(
		$wp_customize, 'hypercommerce_primary_color_control',
		 	array(
				'label' 	=> __('Primary Color', 'hyper-commerce'),
				'settings'	=> 'hypercommerce_primary_color_setting',
				'section'	=> 'colors',
			)
		)
	);
	$wp_customize->add_setting('hypercommerce_secondary_color_setting',
		array(
			'default' 		=> '#00459C',
			'transport'		=> 'refresh',
			'sanitize_callback'	=>'hypercommerce_sanitize_hex_color',
		)
	);
	$wp_customize->add_control(new WP_Customize_Color_Control(
		$wp_customize, 'hypercommerce_secondary_color_control',
		 	array(
				'label' 	=> __('Secondary Color', 'hyper-commerce'),
				'settings'	=> 'hypercommerce_secondary_color_setting',
				'section'	=> 'colors',
			)
		)
	);


	/**
	 * General Settings
	 */
	$wp_customize->add_panel('hypercommerce_general_panel',
		array(
			'title'	=>	__('Theme Options', 'hyper-commerce'),
			'priority'	=>	99,
		)
	);

	/**
	 * pagination 
	 */
	$wp_customize->add_section('hypercommerce_pagination_section',
		array(
			'title' => __('Pagination', 'hyper-commerce'),
			'priority' => 99,
			'panel' 	  => 'hypercommerce_general_panel'
		)
	);
	$wp_customize->add_setting('hypercommerce_pagination_setting',
		array(
			'default' 			=>'number',
			'sanitize_callback'	=> 'hypercommerce_sanitize_pagination_radio_button',
			'transport'			=> 'refresh',
		)
	);

	$wp_customize->add_control('hypercommerce_pagination_setting',
		array(
			'label'		=> __('Pagination', 'hyper-commerce'),
			'section'	=> 'hypercommerce_pagination_section',
			'type'		=> 'radio',
			'choices'	=> array(
				'number' => __('Number(1,2,3.....)', 'hyper-commerce'),
				'text-pagination'=>__('Text- pagination', 'hyper-commerce'),
			)
		)

	);
	/**
	 * select font family 
	 */
	$wp_customize->add_section('hypercommerce_font_section', 
		array(
			'title'	=> __('Font-Family', 'hyper-commerce'),
			'priority' => 95,
			'panel' 	  => 'hypercommerce_general_panel'
		)
	);
	$wp_customize->add_setting('hypercommerce_header_font_setting',
		array(
			'default'		=> 'opensans',
			'sanitize_callback'	=>'hypercommerce_sanitize_font',
			'transport'		=>'refresh',
		)
	);
	$wp_customize->add_control('hypercommerce_header_font_setting',
		array(
			'label'			=> __('Header Font', 'hyper-commerce'),
			'description'	=> __('this custom drop down enables to change the fonts of widgets-headers, menus, post-title, button-text', 'hyper-commerce'),
			'section'		=> 'hypercommerce_font_section',
			'type'			=> 'select',
			'choices'		=> array(
				'opensans'	=> __('OpenSans','hyper-commerce'),
				'roboto'	=> __('Roboto', 'hyper-commerce'),
				'ubuntu'	=> __('Ubuntu', 'hyper-commerce'),
				'lora'		=> __('Lora', 'hyper-commerce'),
				'poiretone'	=> __('Poiret One', 'hyper-commerce'),
				'firasans'	=>	__('Fira Sans', 'hyper-commerce')
			)
		)
	);
	$wp_customize->add_setting('hypercommerce_body_font_setting',
		array(
			'default'		=> 'firasans',
			'sanitize_callback'	=>'hypercommerce_sanitize_font',
			'transport'		=>'refresh',
		)
	);
	$wp_customize->add_control('hypercommerce_body_font_setting',
		array(
			'label'			=> __('Body Font', 'hyper-commerce'),
			'description'	=> __('this custom drop down enables to change the fonts of widgets-content, post-content, comments', 'hyper-commerce'),
			'section'		=> 'hypercommerce_font_section',
			'type'			=> 'select',
			'choices'		=> array(
				'opensans'	=> __('OpenSans','hyper-commerce'),
				'roboto'	=> __('Roboto', 'hyper-commerce'),
				'ubuntu'	=> __('Ubuntu', 'hyper-commerce'),
				'lora'		=> __('Lora', 'hyper-commerce'),
				'poiretone'	=> __('Poiret One', 'hyper-commerce'),
				'firasans'	=>	__('Fira Sans', 'hyper-commerce')
			)
		)
	);
	/**
 * Bread Crumb
 */
	$wp_customize->add_section('hypercommerce_breadcrumb_section', 
		array(
			'title'	=> __('Breadcrumb', 'hyper-commerce'),
			'priority' => 95,
			'panel' 	  => 'hypercommerce_general_panel'
		)
	);

	//Checkbox
	$wp_customize->add_setting( 'hypercommerce_breadcrumb_setting', array(
		'default'           => 0,
		'sanitize_callback' => 'hypercommerce_sanitize_checkbox',
	) );

	$wp_customize->add_control('hypercommerce_breadcrumb_setting', array(
		'label'	=>	__('Display Breadcrumb', 'hyper-commerce'),
		'section'	=>	'hypercommerce_breadcrumb_section',
		'type'	=>	'checkbox',
		)
	);

	/**
 		* Search Bar
 		*/
	$wp_customize->add_section('hypercommerce_searchbar_section', 
		array(
			'title'	=> __('Searchbar', 'hyper-commerce'),
			'priority' => 95,
			'panel' 	  => 'hypercommerce_general_panel'
		)
	);

	//Checkbox
	$wp_customize->add_setting( 'hypercommerce_searchbar_setting', array(
		'default'           => 0,
		'sanitize_callback' => 'hypercommerce_sanitize_checkbox',
	) );

	$wp_customize->add_control('hypercommerce_searchbar_setting', array(
		'label'	=>	__('Display Searchbar on the header', 'hyper-commerce'),
		'section'	=>	'hypercommerce_searchbar_section',
		'type'	=>	'checkbox',
		)
	);

	/**
	 * Footer Options
	 */

	$wp_customize->add_section( 'hypercommerce_footer_options', array(
		'title'			=>	__('Footer Options', 'hyper-commerce'),
		'panel'			=>	'hypercommerce_general_panel'
	) );

	//Footer Background
	$wp_customize->add_setting('hypercommerce_footer_bg_color_setting',
		array(
			'default' 		=> '#797979',
			'transport'		=> 'refresh',
			'sanitize_callback'	=>'hypercommerce_sanitize_hex_color',
		)
	);

	$wp_customize->add_control(new WP_Customize_Color_Control(
		$wp_customize, 'hypercommerce_footer_bg_color_control',
			array(
				'label' 	=> __('Footer Background Color', 'hyper-commerce'),
				'section'	=> 'hypercommerce_footer_options',
				'settings'	=>	'hypercommerce_footer_bg_color_setting'
				
			)
		)
	);

	//Font color
	$wp_customize->add_setting('hypercommerce_footer_font_color_setting',
		array(
			'default' 		=> '#FFFFFF',
			'transport'		=> 'refresh',
			'sanitize_callback'	=>'hypercommerce_sanitize_hex_color',
		)
	);

	$wp_customize->add_control(new WP_Customize_Color_Control(
		$wp_customize, 'hypercommerce_footer_font_color_setting',
			array(
				'label' 	=> __('Footer Text Color', 'hyper-commerce'),
				'section'	=> 'hypercommerce_footer_options',
			)
		)
	);

}
add_action( 'customize_register', 'hypercommerce_customize_register' );


/**
 * Information links section
 */
function hypercommerce_customizer_theme_info( $wp_customize ) {

	$wp_customize->add_section( 'hypercommerce_theme_info' , array(
		'title'       => __( 'Information Links' , 'hyper-commerce' ),
		'priority'    => 6,
		));

	$wp_customize->add_setting('hypercommerce_info_theme',array(
		'default' => '',
		'sanitize_callback' => 'wp_kses_post',
		));
    
    $theme_info = '';
    $theme_info .= '<span class="sticky_info_row"><a href="' . esc_url( 'http://demo.themethread.com/hypercommerce/' ) . '" target="_blank">' . esc_html__( 'View demo', 'hyper-commerce' ) . '</a></span>';
	$theme_info .= '<span class="sticky_info_row"><a href="' . esc_url( 'http://themethread.com/article/hypercommerce-documentation/' ) . '" target="_blank">' . esc_html__( 'View documentation', 'hyper-commerce' ) . '</a></span>';
    $theme_info .= '<span class="sticky_info_row"><a href="' . esc_url( 'http://themethread.com/support/' ) . '" target="_blank">' . esc_html__( 'Support Ticket', 'hyper-commerce' ) . '</a></span>';
	$theme_info .= '<span class="sticky_info_row"><a href="' . esc_url( 'http://themethread.com/theme/hypercommerce/' ) . '" target="_blank">' . esc_html__( 'More Details', 'hyper-commerce' ) . '</a></span>';
	

	$wp_customize->add_control( new hypercommerce_Theme_Info( $wp_customize ,'hypercommerce_info_theme',array(
		'label' => esc_html__( 'About hypercommerce' , 'hyper-commerce' ),
		'section' => 'hypercommerce_theme_info',
		'description' => $theme_info
		)));

	$wp_customize->add_setting('hypercommerce_info_more_theme',array(
		'default' => '',
		'sanitize_callback' => 'wp_kses_post',
		));
}
add_action( 'customize_register', 'hypercommerce_customizer_theme_info' );

if( class_exists( 'WP_Customize_control' ) ){

	class hypercommerce_Theme_Info extends WP_Customize_Control
	{
    	/**
       	* Render the content on the theme customizer page
       	*/
       	public function render_content()
       	{
       		?>
       		<label>
       			<strong class="customize-text_editor"><?php echo esc_html( $this->label ); ?></strong>
       			<br />
       			<span class="customize-text_editor_desc">
       				<?php echo wp_kses_post( $this->description ); ?>
       			</span>
       		</label>
       		<?php
       	}
    }//editor close
    
}//class close

if( class_exists( 'WP_Customize_Section' ) ) :
	/**
	 * Adding Go to Pro Section in Customizer
	 * https://github.com/justintadlock/trt-customizer-pro
	 */
	class Hypercommerce_Customize_Section_Pro extends WP_Customize_Section {
	
		/**
		 * The type of customize section being rendered.
		 *
		 * @since  1.0.0
		 * @access public
		 * @var    string
		 */
		public $type = 'pro-section';
	
		/**
		 * Custom button text to output.
		 *
		 * @since  1.0.0
		 * @access public
		 * @var    string
		 */
		public $pro_text = '';
	
		/**
		 * Custom pro button URL.
		 *
		 * @since  1.0.0
		 * @access public
		 * @var    string
		 */
		public $pro_url = '';
	
		/**
		 * Add custom parameters to pass to the JS via JSON.
		 *
		 * @since  1.0.0
		 * @access public
		 * @return void
		 */
		public function json() {
			$json = parent::json();
	
			$json['pro_text'] = $this->pro_text;
			$json['pro_url']  = esc_url( $this->pro_url );
	
			return $json;
		}
	
		/**
		 * Outputs the Underscore.js template.
		 *
		 * @since  1.0.0
		 * @access public
		 * @return void
		 */
		protected function render_template() { ?>
	
			<li id="accordion-section-{{ data.id }}" class="accordion-section control-section control-section-{{ data.type }} cannot-expand">
	
				<h3 class="accordion-section-title">
					{{ data.title }}
	
					<# if ( data.pro_text && data.pro_url ) { #>
						<a href="{{ data.pro_url }}" class="button button-secondary alignright" target="_blank">{{ data.pro_text }}</a>
					<# } #>
				</h3>
			</li>
		<?php }
	}
	endif;

	add_action( 'customize_register', 'hypercommerce_sections_pro' );
function hypercommerce_sections_pro( $manager ) {
	// Register custom section types.
	$manager->register_section_type( 'Hypercommerce_Customize_Section_Pro' );

	// Register sections.
	$manager->add_section(
		new Hypercommerce_Customize_Section_Pro(
			$manager,
			'hypercommerce_view_pro',
			array(
				'title'    => esc_html__( 'Pro Available', 'hyper-commerce' ),
                'priority' => 5,
				'pro_text' => esc_html__( 'VIEW PRO THEME', 'hyper-commerce' ),
				'pro_url'  => 'https://themethread.com/theme/hypercommerce-pro/'
			)
		)
	);
}

/**
 * sanitization for pagination radio button
 */
function hypercommerce_sanitize_pagination_radio_button($input){
	$valid = array( 'number', 'text-pagination' );

	if ( in_array( $input, $valid, true ) ) {
		return $input;
	}

	return 'number';
}

/* Sanitization Functions
 * 
 * @link https://github.com/WPTRT/code-examples/blob/master/customizer/sanitization-callbacks.php 
 */
     function hypercommerce_sanitize_checkbox( $checked ){
        // Boolean check.
        return ( ( isset( $checked ) && true == $checked ) ? true : false );
     }

/**
 * sanitization for fonts
 */
function hypercommerce_sanitize_font( $value ) {
    $choices = array(
            'ubuntu'         => 'Ubuntu',
            'poiretone'        => 'Poiret One',
            'opensans' => 'Open Sans',
            'roboto'          => 'Roboto',
            'lora'       => 'Lora',
            'firasans'		=>	'Fira Sans',
            );
    $valid = array_keys( $choices );

    if( ! in_array( $value, $valid) ) {
        $value = 'opensans';
    }

    return $value;
}

/**
 * sanitize select
 */
function hypercommerce_sanitize_select( $input, $setting ) {
    
    // Ensure input is a slug.
    $input = sanitize_key( $input );
    
    // Get list of choices from the control associated with the setting.
    $choices = $setting->manager->get_control( $setting->id )->choices;
    
    // If the input is a valid key, return it; otherwise, return the default.
    return ( array_key_exists( $input, $choices ) ? $input : $setting->default );
}

/**
 * Render the site title for the selective refresh partial.
 *
 * @return void
 */
function hypercommerce_customize_partial_blogname() {
	bloginfo( 'name' );
}

/**
 * Render the site tagline for the selective refresh partial.
 *
 * @return void
 */
function hypercommerce_customize_partial_blogdescription() {
	bloginfo( 'description' );
}

/**
 * Binds JS handlers to make Theme Customizer preview reload changes asynchronously.
 */
function hypercommerce_customize_preview_js() {
	wp_enqueue_script( 'hypercommerce-customizer', get_template_directory_uri() . '/js/customizer.js', array( 'customize-preview' ), '20151215', true );
}
add_action( 'customize_preview_init', 'hypercommerce_customize_preview_js' );


/**
 * Function for sanitizing Hex color 
 */
function hypercommerce_sanitize_hex_color( $color ){
    if ( '' === $color )
        return '';

    // 3 or 6 hex digits, or the empty string.
        if ( preg_match('|^#([A-Fa-f0-9]{3}){1,2}$|', $color ) )    
        return $color;
}
