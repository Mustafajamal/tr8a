<?php 

if( !function_exists( 'hypercommerce_woo_cat_dropdown') ):
	function hypercommerce_woo_cat_dropdown(){
	 $all_categories = get_categories( array(
    'taxonomy' => 'product_cat',
    'hide_empty' => false,
)  );
	 // print_r($all_categories);
	 // die;
	 $woocommerce_main_cat = array();
	 foreach ($all_categories as $cat) {
	    
	        $category_id = $cat->term_id; 
	        $category_name = $cat->name;
	        $temp_data[$category_id] = $category_name;   
	}
	return $temp_data;
}
endif;

if( !function_exists( 'hypercommerce_woo_product_dropdown') ):
	function hypercommerce_woo_product_dropdown(){
    $args = array(
        'post_type'      => 'product',
        // 'posts_per_page'	=> 8,
    );

    $loop = new WP_Query( $args );

    while ( $loop->have_posts() ) : $loop->the_post();
        global $product;
        $prod_array[get_the_ID()] = get_the_title(); 
    endwhile;

    wp_reset_query();
    return $prod_array;
    // print_r($prod_array);
	}
endif;


function hypercommerce_woocommerce_image_dimensions() {
    global $pagenow;

    if ( ! isset( $_GET['activated'] ) || $pagenow != 'themes.php' ) {
        return;
    }

    $catalog = array(
        'width'     => '450',   // px
        'height'    => '600',   // px
        'crop'      => 1        // true
    );

    $single = array(
        'width'     => '600',   // px
        'height'    => '600',   // px
        'crop'      => 1        // true
    );

    $thumbnail = array(
        'width'     => '120',   // px
        'height'    => '120',   // px
        'crop'      => 0        // false
    );

    // Image sizes
    update_option( 'shop_catalog_image_size', $catalog );       // Product category thumbs
    update_option( 'shop_single_image_size', $single );         // Single product image
    update_option( 'shop_thumbnail_image_size', $thumbnail );   // Image gallery thumbs
}

add_action( 'after_switch_theme', 'hypercommerce_woocommerce_image_dimensions', 1 );

/**
 * Remove certain woocommerce default elements
 */
add_action( 'init', 'hypercommerce_remove_wc_actions' );
function hypercommerce_remove_wc_actions() {
    remove_action( 'woocommerce_before_main_content', 'woocommerce_breadcrumb', 20, 0 );
    remove_action('woocommerce_sidebar', 'woocommerce_get_sidebar', 10); 
}

/**
 * Remove related products according to customizer settings
 */             
if( get_theme_mod( 'hypercommerce_woo_related_products_setting', 1 ) == 0 ){
    remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_output_related_products', 20 );
}

/**
 * Define the woocommerce_sidebar callback
 */
add_action( 'woocommerce_sidebar', 'hypercommerce_woocommerce_sidebar', 10, 2 );  
function hypercommerce_woocommerce_sidebar( $woocommerce_get_sidebar, $int=10 ) { 
    if( is_active_sidebar( 'sidebar-woocommerce' ) ) : ?>
    <div class="col-lg-3 col-md-3">
        <div id="secondary" class="widget-area" role="complementary">
            <?php dynamic_sidebar( 'sidebar-woocommerce' ); ?>
        </div>
    </div>
<?php 
    endif;
    };  

/**
 * Woo commerce support and effect
 */
add_action( 'after_setup_theme', 'hypercommerce_woocommerce_support' );
function hypercommerce_woocommerce_support() {
    add_theme_support( 'woocommerce' ); 
    add_theme_support( 'wc-product-gallery-zoom' );
    add_theme_support( 'wc-product-gallery-lightbox' );
}


/**
 * Number of products per row in shop page
 */
if( ! is_front_page() ){
    add_filter('loop_shop_columns', 'hypercommerce_loop_columns');
}
if (!function_exists('hypercommerce_loop_columns')) {
    function hypercommerce_loop_columns() {
        if( ! is_front_page() ){
            return get_theme_mod( 'hypercommerce_woo_number_items_setting', 2 ); // products per row
        }else{
            return 4;
        }
    }
}

/**
 * Update top cart
 */
add_filter( 'add_to_cart_fragments', 'hypercommerce_mini_cart_frags', 8, 1 );
function hypercommerce_mini_cart_frags( $fragments ){

    global $woocommerce;

    ob_start();

    $count = $woocommerce->cart->cart_contents_count;

    if( $count != 0 ){
    woocommerce_mini_cart();

    $mini_cart = ob_get_clean();

    $fragments['.header-cart'] = '<div class="header-cart"><i class="fa fa-shopping-cart" aria-hidden="true"></i><div class="dropdown cart-dropdown"><button class="btn btn-cart dropdown-toggle" type="button" data-toggle="dropdown">'.esc_html(get_theme_mod( 'hypercommerce_woo_mycart_label_setting', 'My Cart' )).'<span class="caret"></span></button><ul class="dropdown-menu" id="mini-cart">'.$mini_cart.'</ul></div></div>';

    // $fragments['ul#mini-cart'] = '<ul class="dropdown-menu" id="mini-cart">'.$mini_cart.'</ul>';
    }else{
        $fragments['.header-cart'] = '<div class="header-cart"></div>';
    }

    return $fragments;
    
}

/**
 * Add body class
 */
    add_filter('body_class', 'hypercommerce_woo_body');
    function hypercommerce_woo_body( $classes ){
        if( is_front_page() || is_cart() || is_checkout() ){
            return array_merge( $classes, array('woocommerce'));
        }else{
            return $classes;
        }
    }

add_filter( 'woocommerce_enqueue_styles', 'hypercommerce_dequeue_styles' );
function hypercommerce_dequeue_styles( $enqueue_styles ) {
    if( is_front_page() ){
        unset( $enqueue_styles['woocommerce-layout'] );  // Remove the layout
        unset( $enqueue_styles['woocommerce-smallscreen'] );  // Remove the layout
    }
    return $enqueue_styles;
}

/**
 * WooCommerce Extra Feature
 * --------------------------
 *
 * Change number of related products on product page
 * Set your own value for 'posts_per_page'
 *
 */ 
add_filter( 'woocommerce_output_related_products_args', 'hypercommerce_related_products_args' );
  function hypercommerce_related_products_args( $args ) {

    if( !is_active_sidebar( 'sidebar-woocommerce' ) ){
        $args['posts_per_page'] = 4;
        ?>
            <style>
                .related ul.products li.product{
                    width: 22.05% !important;
                }
                @media  (min-width:320px) and (max-width:479px){
                    .related ul.products li.product{
                        width: 48% !important;
                    }
                }
                @media (min-width: 320px) and (max-width: 479px){
                    .related ul.products li.product{
                        width: 48% !important;
                    }
                }
                @media (min-width: 480px) and (max-width: 559px){
                    .related ul.products li.product{
                        width: 48% !important;
                    }
                }
                @media (min-width: 600px) and (max-width: 767px){
                    .related ul.products li.product{
                        width: 48% !important;
                    }
                }
                @media (min-width: 768px) and (max-width: 991px){
                    .related ul.products li.product{
                        width: 48% !important;
                    }
                }
                @media (min-width: 992px) and (max-width: 1199px){}
                @media (min-width: 1440px) and (max-width: 1899px){}
                @media (min-width: 1900px) and (max-width: 2500px){}
            </style>
        <?php
    }else{
        $args['posts_per_page'] = 3; // 4 related products  
        ?>
            <style>
                .related ul.products li.product{
                    width: 29.5% !important;
                }
                @media  (min-width:320px) and (max-width:479px){
                    .related ul.products li.product{
                        width: 48% !important;
                    }
                }
                @media (min-width: 320px) and (max-width: 479px){
                    .related ul.products li.product{
                        width: 48% !important;
                    }
                }
                @media (min-width: 480px) and (max-width: 559px){
                    .related ul.products li.product{
                        width: 48% !important;
                    }
                }
                @media (min-width: 600px) and (max-width: 767px){
                    .related ul.products li.product{
                        width: 48% !important;
                    }
                }
                @media (min-width: 768px) and (max-width: 991px){
                    .related ul.products li.product{
                        width: 48% !important;
                    }
                }
            </style>
        <?php 
    }
    //$args['columns'] = 2; // arranged in 2 columns
    return $args;
}