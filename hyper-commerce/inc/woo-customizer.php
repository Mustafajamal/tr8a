<?php
/**
 * hypercommerce woocommerce Theme Customizer
 *
 * @package hypercommerce
 */

function hypercommerce_woo_customize( $wp_customize ){

//Panel for woo commerce customization
	$wp_customize->add_section('hypercommerce_woo_panel',
		array(
			'title'	=>	__('Woocommerce Settings', 'hyper-commerce'),
		)
	);

	//Featured Categories
	$wp_customize->add_section('hypercommerce_woo_featured_section', array(
		'title'	=>	__('Featured Categories Settings', 'hyper-commerce'),
		'panel'	=>	'hypercommerce_homepage_panel'
	));

	$wp_customize->add_setting('hypercommerce_woo_featured_setting', array(
		'default'	=>	'1',
		'sanitize_callback'	=>	'hypercommerce_sanitize_checkbox',
	));

	$wp_customize->add_control('hypercommerce_woo_featured_setting', array(
		'label'	=>	__('Show featured categories', 'hyper-commerce'),
		'section'	=>	'hypercommerce_woo_featured_section',
		'type'	=>	'checkbox'
	));

	$wp_customize->add_setting('hypercommerce_woo_featured_first_list_setting', array(
		'default' => '0',
		'sanitize_callback' => 'hypercommerce_sanitize_select'
	));

	$wp_customize->add_control('hypercommerce_woo_featured_first_list_setting', array(
		'label'	=>	__('First Category', 'hyper-commerce'),
		'section'	=>	'hypercommerce_woo_featured_section',
		'type'	=>	'select',
		'choices'	=>	hypercommerce_woo_cat_dropdown()
	));

	$wp_customize->add_setting('hypercommerce_woo_featured_second_list_setting', array(
		'default' => '0',
		'sanitize_callback' => 'hypercommerce_sanitize_select'
	));

	$wp_customize->add_control('hypercommerce_woo_featured_second_list_setting', array(
		'label'	=>	__('Second Category', 'hyper-commerce'),
		'section'	=>	'hypercommerce_woo_featured_section',
		'type'	=>	'select',
		'choices'	=>	hypercommerce_woo_cat_dropdown()
	));

	$wp_customize->add_setting('hypercommerce_woo_featured_third_list_setting', array(
		'default' => '0',
		'sanitize_callback' => 'hypercommerce_sanitize_select'
	));

	$wp_customize->add_control('hypercommerce_woo_featured_third_list_setting', array(
		'label'	=>	__('Third Category', 'hyper-commerce'),
		'section'	=>	'hypercommerce_woo_featured_section',
		'type'	=>	'select',
		'choices'	=>	hypercommerce_woo_cat_dropdown()
	));

	//Title of section
	$wp_customize->add_setting('hypercommerce_woo_featured_title_setting', array(
		'default'	=>	__('Shop By Categories', 'hyper-commerce'),
		'sanitize_callback'	=>	'sanitize_text_field',
		'transport'	=>	'postMessage'
	));

	$wp_customize->add_control('hypercommerce_woo_featured_title_setting', array(
		'label'	=>	__('Title for the section', 'hyper-commerce'),
		'section'	=>	'hypercommerce_woo_featured_section',
		'type'	=>	'text'
	));

	//Button text
	$wp_customize->add_setting('hypercommerce_woo_featured_button_setting', array(
		'default'	=>	__('Shop', 'hyper-commerce'),
		'sanitize_callback'	=>	'sanitize_text_field',
		'transport'	=>	'postMessage'
	));

	$wp_customize->add_control('hypercommerce_woo_featured_button_setting', array(
		'label'	=>	__('Label for the button', 'hyper-commerce'),
		'section'	=>	'hypercommerce_woo_featured_section',
		'type'	=>	'text'
	));

	//Featured Products
	$wp_customize->add_section('hypercommerce_woo_featured_p_section', array(
		'title'	=>	__('Featured Products Settings', 'hyper-commerce'),
		'panel'	=>	'hypercommerce_homepage_panel'
	));

	$wp_customize->add_setting('hypercommerce_woo_featured_p_setting', array(
		'default'	=>	'1',
		'sanitize_callback'	=>	'hypercommerce_sanitize_checkbox',
	));

	$wp_customize->add_control('hypercommerce_woo_featured_p_setting', array(
		'label'	=>	__('Show featured products', 'hyper-commerce'),
		'section'	=>	'hypercommerce_woo_featured_p_section',
		'type'	=>	'checkbox'
	));

	$wp_customize->add_setting('hypercommerce_woo_featured_p_title_setting', array(
		'default'	=>	__('Featured Products', 'hyper-commerce'),
		'sanitize_callback'	=>	'sanitize_text_field',
		'transport'	=>	'postMessage'
	));

	$wp_customize->add_control('hypercommerce_woo_featured_p_title_setting', array(
		'label'	=>	__('Title for the section', 'hyper-commerce'),
		'section'	=>	'hypercommerce_woo_featured_p_section',
		'type'	=>	'text'
	));

	$wp_customize->add_setting('hypercommerce_woo_featured_p_list_setting', array(
		'default' => '4',
		'sanitize_callback' => 'hypercommerce_sanitize_select'
	));

	$wp_customize->add_control('hypercommerce_woo_featured_p_list_setting', array(
		'label'	=>	__('Number of products to display', 'hyper-commerce'),
		'section'	=>	'hypercommerce_woo_featured_p_section',
		'type'	=>	'select',
		'choices'	=>	array(
			'4' => __('Four', 'hyper-commerce'),
			'8'	=>	__('Eight', 'hyper-commerce'),
			'12'	=>	__('Twelve', 'hyper-commerce')
		)
	));

	//New Arrivals
	$wp_customize->add_section('hypercommerce_woo_arrivals_section', array(
		'title'	=>	__('New Arrivals Section Settings', 'hyper-commerce'),
		'panel'	=>	'hypercommerce_homepage_panel'
	));

	$wp_customize->add_setting('hypercommerce_woo_arrivals_setting', array(
		'default'	=>	'1',
		'sanitize_callback'	=>	'hypercommerce_sanitize_checkbox',
	));

	$wp_customize->add_control('hypercommerce_woo_arrivals_setting', array(
		'label'	=>	__('Show new arrivals section', 'hyper-commerce'),
		'section'	=>	'hypercommerce_woo_arrivals_section',
		'type'	=>	'checkbox'
	));

	$wp_customize->add_setting('hypercommerce_woo_arrivals_title_setting', array(
		'default'	=>	__('New Arrivals', 'hyper-commerce'),
		'sanitize_callback'	=>	'sanitize_text_field',
		'transport'	=>	'postMessage'
	));

	$wp_customize->add_control('hypercommerce_woo_arrivals_title_setting', array(
		'label'	=>	__('Title for the section', 'hyper-commerce'),
		'section'	=>	'hypercommerce_woo_arrivals_section',
		'type'	=>	'text'
	));

	$wp_customize->add_setting('hypercommerce_woo_arrivals_list_setting', array(
		'default' => '4',
		'sanitize_callback' => 'hypercommerce_sanitize_select'
	));

	$wp_customize->add_control('hypercommerce_woo_arrivals_list_setting', array(
		'label'	=>	__('Number of products to display', 'hyper-commerce'),
		'section'	=>	'hypercommerce_woo_arrivals_section',
		'type'	=>	'select',
		'choices'	=>	array(
			'4' => __('Four', 'hyper-commerce'),
			'8'	=>	__('Eight', 'hyper-commerce'),
			'12'	=>	__('Twelve', 'hyper-commerce')
		)
	));


	//Slider Settings
	$wp_customize->add_section('hypercommerce_woo_slider_section', array(
		'title'	=>	__('Slider Settings', 'hyper-commerce'),
		'panel'	=>	'hypercommerce_homepage_panel'
	));

	$wp_customize->add_setting('hypercommerce_woo_slider_setting', array(
		'default'	=>	'1',
		'sanitize_callback'	=>	'hypercommerce_sanitize_checkbox',
	));

	$wp_customize->add_control('hypercommerce_woo_slider_setting', array(
		'label'	=>	__('Show slider', 'hyper-commerce'),
		'section'	=>	'hypercommerce_woo_slider_section',
		'type'	=>	'checkbox'
	));

	$wp_customize->add_setting('hypercommerce_woo_slider_tag_setting', array(
		'sanitize_callback'	=>	'sanitize_text_field'
	));

	$wp_customize->add_control('hypercommerce_woo_slider_tag_setting', array(
		'label'	=>	__('Slider Tag', 'hyper-commerce'),
		'section'	=>	'hypercommerce_woo_slider_section',
		'type'	=>	'text'
	));

	//Number of items per row in shop
	$wp_customize->add_setting('hypercommerce_woo_number_items_setting', array(
		'default'	=>	'2',
		'sanitize_callback'	=>	'hypercommerce_sanitize_select',

		));

	$wp_customize->add_control('hypercommerce_woo_number_items_setting', array(
		'label'	=>	__('Display number of products', 'hyper-commerce'),
		'section'	=>	'hypercommerce_woo_panel',
		'type'	=>	'select',
		'choices'	=>	array(
			'2'	=>	__('Two', 'hyper-commerce'),
			'3'	=>	__('Three', 'hyper-commerce'),
			'4'	=>	__('Four', 'hyper-commerce')
			)
		)
	);

	//Button Label
	$wp_customize->add_setting('hypercommerce_woo_slider_button_label', array(
		'default'	=>	__('See Details', 'hyper-commerce'),
		'sanitize_callback'	=>	'sanitize_text_field',
		'transport'	=> 'postMessage'
	));

	$wp_customize->add_control('hypercommerce_woo_slider_button_label', array(
		'label'	=>	__('Slider Button Label', 'hyper-commerce'),
		'section'	=>	'hypercommerce_woo_slider_section',
		'type'	=>	'text'
	));


	//Related products
	$wp_customize->add_setting('hypercommerce_woo_related_products_setting', array(
		'default'	=>	'1',
		'sanitize_callback'	=>	'hypercommerce_sanitize_checkbox',
		));

	$wp_customize->add_control('hypercommerce_woo_related_products_setting', array(
		'label'	=>	__('Show related products', 'hyper-commerce'),
		'section'	=>	'hypercommerce_woo_panel',
		'type'	=>	'checkbox'
	));

	//My card header dropdown
	$wp_customize->add_setting('hypercommerce_woo_mycart_dropdown_setting', array(
		'default'	=>	'1',
		'sanitize_callback'	=>	'hypercommerce_sanitize_checkbox',
		));

	$wp_customize->add_control('hypercommerce_woo_mycart_dropdown_setting', array(
		'label'	=>	__('Show mycart dropdown in header', 'hyper-commerce'),
		'section'	=>	'hypercommerce_woo_panel',
		'type'	=>	'checkbox'
	));

	//My Cart Label
	$wp_customize->add_setting('hypercommerce_woo_mycart_label_setting', array(
		'default'	=>	__('My Cart', 'hyper-commerce'),
		'sanitize_callback'	=>	'sanitize_text_field',
		'transport'	=> 'postMessage'
	));

	$wp_customize->add_control('hypercommerce_woo_mycart_label_setting', array(
		'label'	=>	__('Header Cart Label', 'hyper-commerce'),
		'section'	=>	'hypercommerce_woo_panel',
		'type'	=>	'text'
	));

	//My card header dropdown
	$wp_customize->add_setting('hypercommerce_woo_mycart_dropdown_setting', array(
		'default'	=>	'0',
		'sanitize_callback'	=>	'hypercommerce_sanitize_checkbox',
		));

	$wp_customize->add_control('hypercommerce_woo_mycart_dropdown_setting', array(
		'label'	=>	__('Show mycart dropdown in header', 'hyper-commerce'),
		'section'	=>	'hypercommerce_woo_panel',
		'type'	=>	'checkbox'
	));
}
add_action( 'customize_register', 'hypercommerce_woo_customize' );
