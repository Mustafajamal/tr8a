<?php
/**
 * Dynamic styles
 *
 * @package hypercommerce
 */

function hypercommerce_customize_css(){

	$primarycolor = get_theme_mod('hypercommerce_primary_color_setting', '#FF4B59');
	$secondarycolor = get_theme_mod('hypercommerce_secondary_color_setting', '#00459C');
	$hypercommerce_headerfont = get_theme_mod('hypercommerce_header_font_setting', 'opensans');
	$hypercommerce_bodyfont = get_theme_mod('hypercommerce_body_font_setting', 'firasans');
	$footerBgColor = get_theme_mod( 'hypercommerce_footer_bg_color_setting', '#797979' );
	$footerTextColor = get_theme_mod( 'hypercommerce_footer_font_color_setting', '#FFFFFF' ); 
?>
		<style type = "text/css">
		/*Primary Color*/
			.header-bottom{ 
				background: <?php echo esc_attr($primarycolor); ?>;
				border-top: 1px solid <?php echo esc_attr(hypercommerce_hextorgba( $primarycolor, 0.3)); ?>;
				border-bottom: 1px solid <?php echo esc_attr(hypercommerce_hextorgba( $primarycolor, 0.3)); ?>; 
			}
			.widget-area .widget ul li:hover { background-color: <?php echo esc_attr($primarycolor); ?> }
			.widget-area .widget.woocommerce ul li:hover { background-color: <?php echo esc_attr($primarycolor); ?> }
			.site-footer .widget_nav_menu ul li:hover{ background-color: unset; }
			.navigation.pagination .nav-links .current{ background-color: <?php echo esc_attr($primarycolor); ?> }
			.comment-list .reply a{ color: <?php echo esc_attr($primarycolor); ?> }
			.entry-content blockquote{ border-left: 3px solid <?php echo esc_attr($primarycolor); ?> }
			.entry-content table thead{ background-color: <?php echo esc_attr($primarycolor); ?>  }
			.woocommerce .products .product .add_to_cart_button{
				background: <?php echo esc_attr(hypercommerce_hextorgba( $primarycolor, 0.9 )); ?>
			}
			.woocommerce nav.woocommerce-pagination ul li a:focus, .woocommerce nav.woocommerce-pagination ul li a:hover, .woocommerce nav.woocommerce-pagination ul li span.current{
				background-color: <?php echo esc_attr($primarycolor); ?>
			}
			.woocommerce .products .text-holder .amount{
				color: <?php echo esc_attr($primarycolor); ?>
			}
			.caption .btn-shop{
				/*background:  <?php //echo esc_attr($primarycolor); ?>	*/
			}
			#site-navigation ul ul{
				background: <?php echo esc_attr($primarycolor); ?>
			}
			.woocommerce a.added_to_cart{
				background: <?php echo esc_attr( $primarycolor ); ?>
			}
			.woocommerce ul.products li.product .onsale{
				background: <?php echo esc_attr( $primarycolor ); ?>
			}
			.caption .btn-shop{
				background: <?php echo esc_attr( $primarycolor ); ?>
			}
			.entry-content table th{
				background-color: <?php echo esc_attr( $primarycolor ); ?>
			}
			.woocommerce .widget_price_filter .ui-slider .ui-slider-range{
				background-color: <?php echo esc_attr( $primarycolor ); ?>
			}
			.woocommerce .widget_price_filter .ui-slider .ui-slider-handle{
				background-color: <?php echo esc_attr( $primarycolor ); ?>
			}
			.arrival .arrival-card-wrapper .card-wrapper .text-holder span {
				color: <?php echo esc_attr( $primarycolor ); ?>
			}

		/*Secondary Color	*/
			.site-header .header-top .search-form .search-submit{ 
				background-color: <?php echo esc_attr($secondarycolor); ?>;
				border: 1px solid <?php echo esc_attr($secondarycolor); ?>;
			  }
			.post .entry-footer .btn.read-more{ background-color: <?php echo esc_attr($secondarycolor); ?> }
			.widget-area .widget .widget-title{ background-color: <?php echo esc_attr($secondarycolor); ?> }
			.site-footer .widget .widget-title{ background-color: unset; }
			.widget-area .widget .search-form button, html input[type="button"], input[type="reset"], input[type="submit"]{ background-color: <?php echo esc_attr($secondarycolor); ?> }
			.comment-respond .comment-form .submit{ background: <?php echo esc_attr($secondarycolor); ?> }
			.woocommerce-info{
				border-left-color: <?php echo esc_attr($secondarycolor); ?>; 
			}
			.woocommerce .widget_shopping_cart .buttons a, .woocommerce.widget_shopping_cart .buttons a{
				background: <?php echo esc_attr($secondarycolor); ?>
			}
			.shop-card .text-holder .btn.shop{
				background: <?php echo esc_attr($secondarycolor); ?>;
			}
			.woocommerce #respond input#submit.alt, .woocommerce a.button.alt, .woocommerce button.button.alt, .woocommerce input.button.alt{
				background-color: <?php echo esc_attr($secondarycolor); ?>
			}
			.woocommerce .header-cart .button{
				background-color: <?php echo esc_attr($secondarycolor); ?>
			}
			.woocommerce div.product form.cart .button{
				background-color: <?php echo esc_attr($secondarycolor); ?>; 
			}
			.woocommerce .cart .coupon .button, .woocommerce .cart .coupon input.button{
				background: <?php echo esc_attr($secondarycolor); ?>;  
			}
			.woocommerce .widget_price_filter .price_slider_wrapper .ui-widget-content{
				background-color: <?php echo esc_attr( $secondarycolor ); ?>
			}
			.woocommerce #review_form #respond .form-submit input{
				background-color: <?php echo esc_attr( $secondarycolor ); ?>
			}
			.woocommerce a.button, .woocommerce button.button, .woocommerce input.button{
				background-color: <?php echo esc_attr( $secondarycolor ); ?>
			}

			/* Footer Color */
			<?php
				if( $footerTextColor != '#FFFFFF' ){ ?>
					.site-footer .woocommerce .star-rating span::before {
						color: <?php echo esc_attr( $footerTextColor); ?> !important;
					}
				<?php }
			?>
			.site-footer .holder {
				background-color: <?php echo esc_attr( $footerBgColor ); ?>
			}
			.site-footer .widget .widget-title, .site-footer .widget.widget_calendar table td, .site-footer .widget.widget_calendar table th, .site-footer .widget.widget_calendar table caption, .site-footer .widget-area .widget_shopping_cart .total  {
				color: <?php echo esc_attr( $footerTextColor); ?>
			}
			.site-footer .woocommerce .widget_shopping_cart .total, .site-footer .woocommerce.widget_shopping_cart .total {
				border-top-color: <?php echo esc_attr( $footerTextColor); ?>
			}
			.site-footer .widget.widget_calendar table tbody td, .site-footer .widget.widget_calendar table td a {
				border-color: <?php echo esc_attr( $footerTextColor); ?>
			}
			.site-footer .widget-area .widget ul li a, .site-footer .woocommerce.widget_product_categories .product-categories .cat-item span {
				color: <?php echo esc_attr( $footerTextColor); ?>
			}
			.site-footer .widget-area a, .site-footer .woocommerce ul.product_list_widget li span  {
				color: <?php echo esc_attr( $footerTextColor); ?> !important;
			}
			.site-footer ul li, site-footer a, site-footer span, .site-footer .widget-area, .site-footer .textwidget p {
				color: <?php echo esc_attr( $footerTextColor); ?> !important;
			}

		</style>

	<?php
	/** 
	* for header font
	*/
	if($hypercommerce_headerfont == 'firasans'){
		$headerfont = "Fira Sans";
	}
	elseif($hypercommerce_headerfont == 'opensans'){
		$headerfont = "Open Sans";
	}
	elseif($hypercommerce_headerfont == 'roboto'){
		$headerfont = "Roboto";
	}
	elseif($hypercommerce_headerfont == 'lora'){
		$headerfont = "Lora";
	}
	elseif($hypercommerce_headerfont == 'poiretone'){
		$headerfont = "Poiret One";
	}
	elseif($hypercommerce_headerfont == 'ubuntu'){
		$headerfont = "Ubuntu";
	}
?>

	<style type="text/css">
	#secondary .widget .widget-title{font-family: '<?php echo esc_attr($headerfont);?>';}
	.site-footer .footer-t .widget-title{ font-family: '<?php echo esc_attr($headerfont);?>';}
	#site-navigation ul{font-family: '<?php echo esc_attr($headerfont);?>';}
	#site-navigation ul ul{font-family:'<?php echo esc_attr($headerfont);?>';}
	.site-header .secondary-menu ul{font-family:'<?php echo esc_attr($headerfont);?>';}
	.post .entry-title,.page .entry-title{font-family:'<?php echo esc_attr($headerfont);?>';}
	.post .entry-content .btn-readmore{font-family:'<?php echo esc_attr($headerfont);?>';}
	.entry-header .entry-title{font-family:'<?php echo esc_attr($headerfont);?>';}
	.page-header .archive-title,  .archive-description{font-family:'<?php echo esc_attr($headerfont);?>';}
	.site-main .page-title{font-family:'<?php echo esc_attr($headerfont);?>';}
	#comments .comments-title, #respond .comment-reply-title{font-family: '<?php echo esc_attr($headerfont);?>';}
	#comments .comment-list .fn{font-family: '<?php echo esc_attr($headerfont);?>';}
	.error404 .btn-home{font-family: '<?php echo esc_attr($headerfont);?>';}
	.calendar_wrap th, .calendar_wrap caption, .calendar_wrap tfoot{font-family: '<?php echo esc_attr($headerfont);?>';}
	.entry-content table tbody td{ font-family: '<?php echo esc_attr($headerfont); ?>' ;}
	.entry-content ul li{ font-family: '<?php echo esc_attr($headerfont); ?>' }
	.entry-content ol li{ font-family: '<?php echo esc_attr($headerfont); ?>' }
	h1, h2, h3, h4, h5, h6{ font-family: '<?php echo esc_attr($headerfont); ?>' }
	.site-footer .widget_nav_menu ul li{ font-family: '<?php echo esc_attr($headerfont); ?>' }

	<?php if(hypercommerce_woocommerce_activated()){?>
		<!--woocommerce-->
		.woocommerce .product .summary .entry-title{ font-family:'<?php echo esc_attr($headerfont);?>' !important;}
		.woocommerce-tabs ul.tabs{font-family:'<?php echo esc_attr($headerfont);?>';}
		.woocommerce-tabs .panel h2{font-family:'<?php echo esc_attr($headerfont);?>';}
		.related.products h2{font-family:'<?php echo esc_attr($headerfont);?>';}
		.woocommerce .shop_table th, .cart_totals, .cart_totals tr{ font-family:'<?php echo esc_attr($headerfont);?>';}
		.checkout h3{ font-family:'<?php echo esc_attr($headerfont);?>' !important;}
		#payment ul, #payment input[type="submit"]{font-family:'<?php echo esc_attr($headerfont);?>' !important;}
		.woocommerce-MyAccount-navigation{font-family:'<?php echo esc_attr($headerfont);?>';}

		<!--shop page-->
		<?php 
			$shop_prod = esc_attr(get_theme_mod( 'hypercommerce_woo_number_items_setting', 2 ));
			if($shop_prod == 2){
				$width_shop = '48.05%';
		}elseif($shop_prod == 3){
				$width_shop = '30.8%';
		}elseif($shop_prod == 4){ 
				$width_shop = '22.05%';
		} 
		?>
		.archive .woocommerce ul.products li.product, .woocommerce-page ul.products li.product{
			width: <?php echo esc_attr($width_shop);?>;
		}


	<?php
	}
	?>
	</style>
	<?php
	/** 
	* for body font
	*/

	if($hypercommerce_bodyfont == 'opensans'){
		$bodyfont = "Open Sans";
	}
	elseif($hypercommerce_bodyfont == 'roboto'){
		$bodyfont = "Roboto";
	}
	elseif($hypercommerce_bodyfont == 'lora'){
		$bodyfont = "Lora";
	}
	elseif($hypercommerce_bodyfont == 'poiretone'){
		$bodyfont = "Poiret One";
	}
	elseif($hypercommerce_bodyfont == 'ubuntu'){
		$bodyfont = "Ubuntu";
	}
	elseif($hypercommerce_bodyfont == 'firasans'){
		$bodyfont = "Fira Sans";
	}
	?>
	<style type="text/css">
	body{ font-family: '<?php echo esc_attr($bodyfont); ?>' }
	#comments .comment-list .comment-body .comment-content, #comments .comment-list .comment-body .comment-metadata{font-family: '<?php echo esc_attr($bodyfont); ?>';}
	#comments .comment-list .comment-body .reply{font-family: '<?php echo esc_attr($bodyfont); ?>';}
	.comment-form p, .comment-form textarea, .comment-form .form-submit input[type="submit"]{font-family: '<?php echo esc_attr($bodyfont); ?>';}
	.site-footer .footer-t ul li{font-family:'<?php echo esc_attr($bodyfont); ?>';}
	.post .entry-header .meta-info{font-family: '<?php echo esc_attr($bodyfont); ?>';}
	.post .entry-content p, .page .entry-content p{font-family: '<?php echo esc_attr($bodyfont); ?>';}
	.site-footer .site-info{font-family: '<?php echo esc_attr($bodyfont); ?>';}
	.widget ul{font-family: '<?php echo esc_attr($bodyfont); ?>';}
	.widget .tagcloud{font-family: '<?php echo esc_attr($bodyfont); ?>';}
	.widget .textwidget{font-family: '<?php echo esc_attr($bodyfont); ?>';}
	.entry-header .entry-meta{font-family:'<?php echo esc_attr($bodyfont); ?>';}
	.entry-summary p, .entry-footer{font-family:'<?php echo esc_attr($bodyfont); ?>';}
	.page-content p{font-family:'<?php echo esc_attr($bodyfont); ?>';}
	.wp-caption-text, .custom-html-widget{font-family:'<?php echo esc_attr($bodyfont); ?>';}
	.calendar_wrap td{font-family:'<?php echo esc_attr($bodyfont); ?>';}


	<?php if(hypercommerce_woocommerce_activated()){?>
		<!-- woocommerce-->
		.woocommerce .woocommerce-result-count{font-family:'<?php echo esc_attr($bodyfont); ?>';}
		.products, .product .price, .products .button{font-family:'<?php echo esc_attr($bodyfont); ?>';}
		.woocommerce-product-details__short-description p{font-family:'<?php echo esc_attr($bodyfont); ?>';}
		.cart .button{font-family:'<?php echo esc_attr($bodyfont); ?>';}
		.product_meta, .out-of-stock{font-family:'<?php echo esc_attr($bodyfont); ?>';}
		.woocommerce-tabs .panel p{font-family:'<?php echo esc_attr($bodyfont); ?>';}
		.woocommerce a.button, .woocommerce button.button, .woocommerce input.button{font-family:'<?php echo esc_attr($bodyfont); ?>';}
		.widget_shopping_cart_content p{font-family:'<?php echo esc_attr($bodyfont); ?>';}
		.woocommerce .shop_table tr{ font-family:'<?php echo esc_attr($bodyfont); ?>';}
		.woocommerce .shop_table .actions input[type="submit"]{ font-family:'<?php echo esc_attr($bodyfont); ?>' !important;}
		.woocommerce .shop_table .actions input[type="text"]{ font-family:'<?php echo esc_attr($bodyfont); ?>' !important;}
		.woocommerce-info{ font-family:'<?php echo esc_attr($bodyfont); ?>' !important;}
	

	<?php } echo '</style>';

}

add_action( 'wp_head', 'hypercommerce_customize_css');
?>