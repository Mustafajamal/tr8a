<?php
/**
 * The sidebar containing the main widget area
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package hyper-commerce
 */

if( hypercommerce_woocommerce_activated() && ( is_cart() || is_checkout() ) ){
	if( ! is_active_sidebar( 'sidebar-woocommerce' ) ){
		return;
	}
}

if ( ! is_active_sidebar( 'sidebar-1' ) && ! is_active_sidebar( 'sidebar-woocommerce' ) ) {
	return;
}

?>
<div class="col-lg-3 col-md-3">
	<div id="secondary" class="widget-area" role="complementary">
			<?php 
			if( hypercommerce_woocommerce_activated() && ( is_cart() || is_checkout() ) ){
      		dynamic_sidebar( 'sidebar-woocommerce' );     
        	}	
			else{
      		dynamic_sidebar( 'sidebar-1' );   	
	        }
			?>
	</div>
</div>