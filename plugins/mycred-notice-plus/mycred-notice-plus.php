<?php
/**
 * Plugin Name: myCRED Notifications Plus
 * Description: Notifications with options to style, show notifications for ranks and badges and optional instant notifications.
 * Version: 1.3.7
 * Author: Gabriel S Merovingi
 * Author URI: http://www.merovingi.com
 * Requires at least: WP 4.0
 * Tested up to: WP 4.8
 * Text Domain: mycred_notice
 * Domain Path: /lang
 * License: Copyrighted
 *
 * Copyright © 2013 - 2017 Gabriel S Merovingi
 * 
 * Permission is hereby granted, to the licensed domain to install and run this
 * software and associated documentation files (the "Software") for an unlimited
 * time with the followning restrictions:
 *
 * - This software is only used under the domain name registered with the purchased
 *   license though the myCRED website (mycred.me). Exception is given for localhost
 *   installations or test enviroments.
 *
 * - This software can not be copied and installed on a website not licensed.
 *
 * - This software is supported only if no changes are made to the software files
 *   or documentation. All support is voided as soon as any changes are made.
 *
 * - This software is not copied and re-sold under the current brand or any other
 *   branding in any medium or format.
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
if ( ! class_exists( 'myCRED_Notice_Plus_Plugin' ) ) :
	final class myCRED_Notice_Plus_Plugin {

		// Plugin Version
		public $version             = '1.3.7';

		// Instnace
		protected static $_instance = NULL;

		// Current session
		public $session             = NULL;

		public $slug                = '';
		public $domain              = '';
		public $plugin              = NULL;
		public $plugin_name         = '';
		protected $update_url       = 'http://mycred.me/api/plugins/';

		/**
		 * Setup Instance
		 * @since 1.3.3
		 * @version 1.0
		 */
		public static function instance() {
			if ( is_null( self::$_instance ) ) {
				self::$_instance = new self();
			}
			return self::$_instance;
		}

		/**
		 * Not allowed
		 * @since 1.3.3
		 * @version 1.0
		 */
		public function __clone() { _doing_it_wrong( __FUNCTION__, 'Cheatin&#8217; huh?', '1.0' ); }

		/**
		 * Not allowed
		 * @since 1.3.3
		 * @version 1.0
		 */
		public function __wakeup() { _doing_it_wrong( __FUNCTION__, 'Cheatin&#8217; huh?', '1.0' ); }

		/**
		 * Define
		 * @since 1.3.3
		 * @version 1.0
		 */
		private function define( $name, $value, $definable = true ) {
			if ( ! defined( $name ) )
				define( $name, $value );
		}

		/**
		 * Require File
		 * @since 1.3.3
		 * @version 1.0
		 */
		public function file( $required_file ) {
			if ( file_exists( $required_file ) )
				require_once $required_file;
		}

		/**
		 * Construct
		 * @since 1.3.3
		 * @version 1.0
		 */
		public function __construct() {

			$this->slug        = 'mycred-notice-plus';
			$this->plugin      = plugin_basename( __FILE__ );
			$this->domain      = 'mycred_notice';
			$this->plugin_name = 'myCRED Notifications Plus';

			$this->define_constants();
			$this->includes();
			$this->plugin_updates();

			register_activation_hook( myCRED_NOTICE, 'mycred_note_plus_plugin_activation' );
			register_uninstall_hook(  myCRED_NOTICE, 'mycred_note_plus_plugin_uninstall' );

			add_action( 'mycred_pre_init', array( $this, 'start_up' ) );
			add_action( 'mycred_init',     array( $this, 'load_textdomain' ) );

		}

		/**
		 * Define Constants
		 * @since 1.0
		 * @version 1.0
		 */
		public function define_constants() {

			$this->define( 'MYCRED_NOTICE_VERSION',      $this->version );
			$this->define( 'MYCRED_NOTICE_SLUG',         $this->slug );
			$this->define( 'MYCRED_NOTICE_JS_VERSION',   $this->version );
			$this->define( 'MYCRED_NOTICE_CSS_VERSION',  $this->version );
			$this->define( 'MYCRED_DEFAULT_TYPE_KEY',    'mycred_default' );
			$this->define( 'MYCRED_MIN_SCRIPTS',         false );

			$this->define( 'myCRED_NOTICE',              __FILE__ );
			$this->define( 'MYCRED_NOTICE_ROOT_DIR',     plugin_dir_path( myCRED_NOTICE ) );
			$this->define( 'MYCRED_NOTICE_ASSETS_DIR',   MYCRED_NOTICE_ROOT_DIR . 'assets/' );
			$this->define( 'MYCRED_NOTICE_INCLUDES_DIR', MYCRED_NOTICE_ROOT_DIR . 'includes/' );
			$this->define( 'MYCRED_NOTICE_MODULES_DIR',  MYCRED_NOTICE_ROOT_DIR . 'modules/' );

		}

		/**
		 * Includes
		 * @since 1.0
		 * @version 1.0
		 */
		public function includes() {

			$this->file( MYCRED_NOTICE_INCLUDES_DIR . 'mycred-notice-functions.php' );

		}

		/**
		 * Start
		 * @since 1.0
		 * @version 1.0
		 */
		public function start_up() {

			$this->file( MYCRED_NOTICE_MODULES_DIR . 'mycred-notifications.php' );

			$notice = new myCRED_Notifications();
			$notice->load();

		}

		/**
		 * Load Textdomain
		 * @since 1.0
		 * @version 1.0
		 */
		public function load_textdomain() {

			// Load Translation
			$locale = apply_filters( 'plugin_locale', get_locale(), $this->domain );

			load_textdomain( $this->domain, WP_LANG_DIR . '/' . $this->slug . '/' . $this->domain . '-' . $locale . '.mo' );
			load_plugin_textdomain( $this->domain, false, dirname( $this->plugin ) . '/lang/' );

		}

		/**
		 * Plugin Updates
		 * @since 1.0
		 * @version 1.0
		 */
		public function plugin_updates() {

			add_filter( 'pre_set_site_transient_update_plugins', array( $this, 'check_for_plugin_update' ), 210 );
			add_filter( 'plugins_api',                           array( $this, 'plugin_api_call' ), 210, 3 );
			add_filter( 'plugin_row_meta',                       array( $this, 'plugin_view_info' ), 210, 3 );

		}

		/**
		 * Plugin Update Check
		 * @since 1.0
		 * @version 1.0
		 */
		public function check_for_plugin_update( $checked_data ) {

			global $wp_version;

			if ( empty( $checked_data->checked ) )
				return $checked_data;

			$args = array(
				'slug'    => $this->slug,
				'version' => $this->version,
				'site'    => site_url()
			);
			$request_string = array(
				'body'       => array(
					'action'     => 'version', 
					'request'    => serialize( $args ),
					'api-key'    => md5( get_bloginfo( 'url' ) )
				),
				'user-agent' => 'WordPress/' . $wp_version . '; ' . get_bloginfo( 'url' )
			);

			// Start checking for an update
			$response = wp_remote_post( $this->update_url, $request_string );

			if ( ! is_wp_error( $response ) ) {

				$result = maybe_unserialize( $response['body'] );

				if ( is_object( $result ) && ! empty( $result ) )
					$checked_data->response[ $this->slug . '/' . $this->slug . '.php' ] = $result;

			}

			return $checked_data;

		}

		/**
		 * Plugin View Info
		 * @since 1.0
		 * @version 1.0
		 */
		public function plugin_view_info( $plugin_meta, $file, $plugin_data ) {

			if ( $file != $this->plugin ) return $plugin_meta;

			$plugin_meta[] = sprintf( '<a href="%s" class="thickbox" aria-label="%s" data-title="%s">%s</a>',
				esc_url( network_admin_url( 'plugin-install.php?tab=plugin-information&plugin=' . $this->slug .
				'&TB_iframe=true&width=600&height=550' ) ),
				esc_attr( __( 'More information about this plugin', 'mycred_notice' ) ),
				esc_attr( $this->plugin_name ),
				__( 'View details', 'mycred_notice' )
			);

			$url     = str_replace( array( 'https://', 'http://' ), '', get_bloginfo( 'url' ) );
			$expires = get_option( 'mycred-premium-' . $this->slug . '-expires', '' );
			if ( $expires != '' ) {

				if ( $expires == 'never' )
					$plugin_meta[] = 'Unlimited License';

				elseif ( absint( $expires ) > 0 ) {

					$days = ceil( ( $expires - current_time( 'timestamp' ) ) / DAY_IN_SECONDS );
					if ( $days > 0 )
						$plugin_meta[] = sprintf(
							'License Expires in <strong%s>%s</strong>',
							( ( $days < 30 ) ? ' style="color:red;"' : '' ),
							sprintf( _n( '1 day', '%d days', $days ), $days )
						);

					$renew = get_option( 'mycred-premium-' . $this->slug . '-renew', '' );
					if ( $days < 30 && $renew != '' )
						$plugin_meta[] = '<a href="' . esc_url( $renew ) . '" target="_blank" class="delete">Renew License</a>';

				}

			}

			else $plugin_meta[] = '<a href="https://mycred.me/about/terms/#product-licenses" target="_blank">No license found for - ' . $url . '</a>';

			return $plugin_meta;

		}

		/**
		 * Plugin New Version Update
		 * @since 1.0
		 * @version 1.0
		 */
		public function plugin_api_call( $result, $action, $args ) {

			global $wp_version;

			if ( ! isset( $args->slug ) || ( $args->slug != $this->slug ) )
				return $result;

			// Get the current version
			$args = array(
				'slug'    => $this->slug,
				'version' => $this->version,
				'site'    => site_url()
			);
			$request_string = array(
				'body'       => array(
					'action'     => 'info', 
					'request'    => serialize( $args ),
					'api-key'    => md5( get_bloginfo( 'url' ) )
				),
				'user-agent' => 'WordPress/' . $wp_version . '; ' . get_bloginfo( 'url' )
			);

			$request = wp_remote_post( $this->update_url, $request_string );

			if ( ! is_wp_error( $request ) )
				$result = maybe_unserialize( $request['body'] );

			if ( $result->license_expires != '' )
				update_option( 'mycred-premium-' . $this->slug . '-expires', $result->license_expires );

			if ( $result->license_renew != '' )
				update_option( 'mycred-premium-' . $this->slug . '-renew',   $result->license_renew );

			return $result;

		}

	}
endif;

function mycred_notice_plus_plugin() {
	return myCRED_Notice_Plus_Plugin::instance();
}
mycred_notice_plus_plugin();
