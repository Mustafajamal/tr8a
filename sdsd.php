<?php
add_shortcode( 'mycred_take', 'mycred_pro_render_take_shortcode' );
function mycred_pro_render_take_shortcode( $atts, $label = 'Give Away' ) {
	extract( shortcode_atts( array(
		'user_id' => '',
		'confirm' => '',
		'amount'  => '12',
		'unique'  => 0,
		'ref'     => 'mycred_take',
		'entry'   => '%plural% lose',
		'done'    => 'You have lost points',
		'ctype'   => 'mycred_default'
	), $atts ) );

	if ( ! is_user_logged_in() || ! function_exists( 'mycred' ) ) return '';

	if ( $user_id == '' )
		$user_id = get_current_user_id();

	// Load essentials
	$user_id = absint( $user_id );
	$mycred = mycred( $ctype );

	// User is excluded = has no balance
	//if ( $mycred->exclude_user( $user_id ) ) return '';

	// Unique check
	//if ( $unique == 1 && $mycred->has_entry( $ref, 0, $user_id, '', $ctype ) ) return '';

	$balance = $mycred->get_users_balance( $user_id, $ctype );

	$output = '';

	// If button was pushed
	if ( isset( $_POST['mycred-take-points-token'] ) && wp_verify_nonce( $_POST['mycred-take-points-token'], 'mycred-deduct-points' . $ref . $ctype ) ) {

		// Deduct
		$mycred->add_creds(
			$ref,
			$user_id,
			0 - $amount,
			$entry
		);

		// Update balance
		$balance = $balance - $amount;

		if ( $done != '' )
			$output .= '<p>' . $done . '</p>';

	}

	// Too low balance
	//if ( $balance < $amount ) return '';

	$onclick = '';
	if ( $confirm != '' )
		$onclick = '
<script type="text/javascript">
( function( $ ) {
	$( "form#mycred-take-shortcode' . $ref . $ctype . '" ).on( "submit", function(){
		if ( ! confirm( \'' . $confirm . '\' ) ) return false;
	});
} )( jQuery );
</script>';

ob_start();
?>
	<form id="<?php echo 'mycred-take-shortcode '.$ref.' '.$ctype;?>"><input type="hidden" name="mycred-take-points-token" value="<?php echo 'mycred-deduct-points '.$ref.' '.$ctype;?>" /><input type="submit" class="button" value="ENTER COMPETITION" style="display:block;"/></form>
	<?php
$result = ob_get_clean();
return $result;
}